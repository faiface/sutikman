package faiface.stickmangine.tests;

import com.badlogic.gdx.*;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;

import faiface.stickmangine.*;
import faiface.stickmangine.balance.Balancer;
import faiface.stickmangine.balance.CenterOfMass;
import faiface.stickmangine.balance.LocoVector;
import faiface.stickmangine.balance.Locomotion;
import faiface.stickmangine.figures.stickman.StickmanBody;
import faiface.stickmangine.figures.stickman.StickmanBrain;
import faiface.stickmangine.figures.stickman.StickmanDef;
import faiface.stickmangine.movement.*;

public class Test implements ApplicationListener, InputProcessor {

    final float ups = 800;
    final float updateDelta = 1 / ups;
    float updateTime = 0;
    float scale = 150;
    float camSpeed = 7f;

    Runtime runtime;

    World world;
    Box2DDebugRenderer renderer;
    OrthographicCamera camera;

    Body staticBody;

    Stick stick1, stick2, stick3, stick4, stick5;
    Point point1, point2, point3, point4;
    Limb limb1, limb2;
    Balancer balancer1, balancer2;
    DirectMove balanceShift1, balanceShift2;
    Bend bend1, bend2;

    Stick stickM, stickA, stickB, stickC;
    Point pointA, pointB, pointC;
    Limb limb;
    Balancer arbBalancer;
    Bend bendA, bendB, bendC;
    SimpleMove simpleMove;
    DirectMove directMove;
    Action action;

    CenterOfMass cm;

    StickmanBody stickmanBody;
    StickmanBrain stickmanBrain;

    float totalTorque = 0;

    @Override
    public void create() {
        runtime = Runtime.getRuntime();

        Gdx.input.setInputProcessor(this);

        world = new World(new Vector2(0, -10f), true);
        renderer = new Box2DDebugRenderer(true, false, false, true, false);
        camera = new OrthographicCamera();

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.KinematicBody;
        bodyDef.position.set(0, -1);
        bodyDef.angle = 0f;

        PolygonShape boxShape = new PolygonShape();
        boxShape.setAsBox(10f, 0.1f, new Vector2(0, 0f), 0);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = boxShape;
        fixtureDef.restitution = 0f;
        fixtureDef.friction = 0.4f;

        staticBody = world.createBody(bodyDef);
        staticBody.createFixture(fixtureDef);

//        boxShape.setAsBox(4f, 0.1f, new Vector2(0, 7f), 0);
//        staticBody.createFixture(fixtureDef);

//        boxShape.setAsBox(0.1f, 4f, new Vector2(-4, 3), 0);
//        staticBody.createFixture(fixtureDef);
//
//        boxShape.setAsBox(0.1f, 4f, new Vector2(4, 3), 0);
//        staticBody.createFixture(fixtureDef);

        StickDef stickDef = new StickDef();
        stickDef.world = world;
        stickDef.filter.groupIndex = -1;
        stickDef.density = 1f;
        stickDef.width = 0.1f;
        stickDef.friction = 0.4f;

        //stickDef.dynamic = false;
        stickDef.density = 50f;
        stickDef.setA(new Vector2(0, 1.5f));
        stickDef.setB(stickDef.A().add(0, -0.9f));
        stick1 = new Stick(stickDef);

        stickDef.dynamic = true;
        stickDef.density = 20f;
        stickDef.setA(stickDef.B());
        stickDef.setB(stickDef.A().add(0, -0.5f));
        stick2 = new Stick(stickDef);

        stickDef.setA(stickDef.B());
        stickDef.setB(stickDef.A().add(0, -0.5f));
        stick3 = new Stick(stickDef);

        stickDef.setA(stick1.B());
        stickDef.setB(stickDef.A().add(0, -0.5f));
        stick4 = new Stick(stickDef);

        stickDef.setA(stickDef.B());
        stickDef.setB(stickDef.A().add(0, -0.5f));
        stick5 = new Stick(stickDef);

        stickDef.filter.groupIndex = -2;
        stickDef.dynamic = false;
        stickDef.setA(new Vector2(-2, 1));
        stickDef.setB(stickDef.A().add(0, -0.1f));
        stickM = new Stick(stickDef);

        stickDef.dynamic = true;
        stickDef.setA(stickDef.B());
        stickDef.setB(stickDef.A().add(0, -0.5f));
        stickA = new Stick(stickDef);

        stickDef.setA(stickDef.B());
        stickDef.setB(stickDef.A().add(0, -0.5f));
        stickB = new Stick(stickDef);

        stickDef.setA(stickDef.B());
        stickDef.setB(stickDef.A().add(0, -0.4f));
        stickC = new Stick(stickDef);

        PointDef pointDef = new PointDef();
        //pointDef.strength = 200;
        pointDef.strength = Float.POSITIVE_INFINITY;
        //pointDef.restingTension = 0f;
        //pointDef.explosiveness = 15f;
        pointDef.explosiveness = 20;
        pointDef.stiffness = 20;

        pointDef.segmentA = stick1;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = stick2;
        pointDef.anchorB = PointDef.Anchor.A;
        point1 = new Point(pointDef);

        pointDef.segmentA = stick2;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = stick3;
        pointDef.anchorB = PointDef.Anchor.A;
        point2 = new Point(pointDef);

        pointDef.segmentA = stick1;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = stick4;
        pointDef.anchorB = PointDef.Anchor.A;
        point3 = new Point(pointDef);

        pointDef.segmentA = stick4;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = stick5;
        pointDef.anchorB = PointDef.Anchor.A;
        point4 = new Point(pointDef);

        pointDef.stiffness = 20f;
        pointDef.segmentA = stickM;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = stickA;
        pointDef.anchorB = PointDef.Anchor.A;
        pointA = new Point(pointDef);

        pointDef.segmentA = stickA;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = stickB;
        pointDef.anchorB = PointDef.Anchor.A;
        pointB = new Point(pointDef);

        pointDef.segmentA = stickB;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = stickC;
        pointDef.anchorB = PointDef.Anchor.A;
        pointC = new Point(pointDef);

        limb1 = new Limb(point1, point2, Limb.Orientation.CW);
        limb2 = new Limb(point3, point4, Limb.Orientation.CW);

        limb = new Limb(pointA, pointB, Limb.Orientation.CCW);
        arbBalancer = new Balancer(new CenterOfMass(stickA.body(), stickB.body(), stickC.body()), limb, 0, 1);

        cm = new CenterOfMass(stick1.body(), stick2.body(), stick3.body(), stick4.body(), stick5.body());
        balancer1 = new Balancer(cm, limb1, 0, 1f);
        balancer2 = new Balancer(cm, limb2, 0, 1f);

        boxShape.dispose();

        StickmanDef stickmanDef = new StickmanDef();
        stickmanDef.world = world;
        stickmanDef.index = -3;
        stickmanDef.restitution = 0;
        stickmanDef.transform.setPosition(new Vector2(2f, -0.5f));
        stickmanDef.height = 2;
        stickmanDef.width = 0.09f;
        stickmanDef.weight = 70;
        stickmanDef.strength = Float.POSITIVE_INFINITY;
        stickmanDef.explosiveness = 37;
        stickmanDef.stiffness = 300;
        stickmanBody = new StickmanBody(stickmanDef);
        stickmanBrain = new StickmanBrain(stickmanBody);
    }

    float delta = 0;
    float angle = 0;
    float totalSpeed = 0;

    @Override
    public void render() {
        float dt = Gdx.graphics.getDeltaTime();

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
            camera.position.add(-camSpeed * dt, 0, 0);
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
            camera.position.add(camSpeed * dt, 0, 0);
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN))
            camera.position.add(0, -camSpeed * dt, 0);
        if (Gdx.input.isKeyPressed(Input.Keys.UP))
            camera.position.add(0, camSpeed * dt, 0);
//        camera.position.set(stick1.B().x, stick1.B().y, 0);
//        camera.rotate(180 - stick1.angle() * MathUtils.radDeg + 90);
        camera.update();
//        camera.rotate(180 + stick1.angle() * MathUtils.radDeg - 90);

        if (!Gdx.input.isKeyPressed(Input.Keys.GRAVE)) {
            for (updateTime += Gdx.input.isKeyPressed(Input.Keys.TAB) ? dt/10 : dt; updateTime >= updateDelta; updateTime -= updateDelta) {
                world.step(updateDelta, 8, 3);
                point1.update(updateDelta);
                point2.update(updateDelta);
                point3.update(updateDelta);
                point4.update(updateDelta);

                pointA.update(updateDelta);
                pointB.update(updateDelta);
                pointC.update(updateDelta);

                String s = "";
                if (bendA != null) {
                    bendA = bendA.update(updateDelta);
                    s += "A:ON  ";
                } else {
                    s += "A:OFF ";
                }
                if (bendB != null) {
                    bendB = bendB.update(updateDelta);
                    s += "B:ON  ";
                } else {
                    s += "B:OFF ";
                }
//                if (bendC != null) {
//                    bendC = bendC.update(updateDelta);
//                    s += "C:ON  ";
//                } else {
//                    s += "C:OFF ";
//                }
//                if (simpleMove != null) {
//                    simpleMove = simpleMove.update(updateDelta);
//                    s += "S:ON  ";
//                } else {
//                    s += "S:OFF ";
//                }
//                if (directMove != null) {
//                    directMove = directMove.update(updateDelta);
//                    s += "D:ON  ";
//                } else {
//                    s += "D:OFF ";
//                }
                if (action != null) {
                    action = action.update(updateDelta);
                    s += "A:ON  ";
                } else {
                    s += "A:OFF ";
                }
                //System.out.printf("\r%s %.3f", s, directMove != null ? directMove.fraction() : 0);

                stickmanBrain.stickmanBody().update(updateDelta);
                stickmanBrain.update(updateDelta);

                cm.setReferenceSpeed(staticBody.getLinearVelocity());
                cm.update(updateDelta);
                balancer1.update(updateDelta);
                balancer2.update(updateDelta);

//                Vector2 cmSpeed = cm.speed().div(1);
//                Vector2 acc = cmSpeed.cpy().div(-0.33f);
//                acc.x = MathUtils.clamp(acc.x, -0.4f * world.getGravity().len(), 0.4f * world.getGravity().len());
//                balancer1.setAcceleration(acc);
//                balancer2.setAcceleration(acc);

                float g = world.getGravity().len();
                float v = cm.speed().x;

                float minA = Float.POSITIVE_INFINITY;

                float h1 = balancer1.comPointerPos().x;
                Locomotion.LocoVector[] lv1 = Locomotion.atVelocity(g, h1, -0.3f, 0, v);
                if (lv1.length > 0 && lv1[0].time() <= 0) {
                    float p = lv1[0].position();
                    float a = MathUtils.clamp((p + 0.3f) * (g / h1), -0.4f * world.getGravity().len(), 0.4f * world.getGravity().len());
                    if (Math.abs(a) < Math.abs(minA)) {
                        minA = a;
                    }
                }

                float h2 = balancer2.comPointerPos().x;
                Locomotion.LocoVector[] lv2 = Locomotion.atVelocity(g, h2, 0.3f, 0, v);
                if (lv2.length > 0 && lv2[0].time() <= 0) {
                    float p = lv2[0].position();
                    float a = MathUtils.clamp((p - 0.3f) * (g / h2), -0.4f * world.getGravity().len(), 0.4f * world.getGravity().len());
                    if (Math.abs(a) < Math.abs(minA)) {
                        minA = a;
                    }
                }

                if (Float.isFinite(minA)) {
                    balancer1.setAcceleration(new Vector2(minA, 0));
                    balancer2.setAcceleration(new Vector2(minA, 0));
                }

                balancer1.setIntensity(balancer1.error() / (MathUtils.PI / 6) * (balancer1.pointerPos().y > 0 ? +1 : -1));
                balancer1.setIntensity(MathUtils.clamp(balancer1.intensity(), 0, 0.9f));
                balancer2.setIntensity(balancer2.error() / (MathUtils.PI / 6) * (balancer2.pointerPos().y > 0 ? +1 : -1));
                balancer2.setIntensity(MathUtils.clamp(balancer2.intensity(), 0, 0.9f));

                if (balanceShift1 != null) balanceShift1 = balanceShift1.update(updateDelta);
                if (balanceShift2 != null) balanceShift2 = balanceShift2.update(updateDelta);
                if (bend1 != null) bend1 = bend1.update(updateDelta);
                if (bend2 != null) bend2 = bend2.update(updateDelta);

                if (balancer1.position(1).y < balancer2.position(1).y) {
                    MovementPoint tmp = balancer1.movementPoint;
                    balancer1.movementPoint = balancer2.movementPoint;
                    balancer2.movementPoint = tmp;
                }

                //TEST
                if ((totalTorque -= 27 * updateDelta) < 0) totalTorque = 0;
                totalTorque += Math.abs((point1.torque() + point2.torque() + point3.torque() + point4.torque()) * updateDelta);

                bend1 = new Bend(balancer1, angle, 1 / MathUtils.PI).setTime(0.1f).start();
                bend2 = new Bend(balancer2, angle, 1 / MathUtils.PI).setTime(0.1f).start();

                Vector2 position1 = new Vector2(0.9f, 0.3f + delta);
                Vector2 position2 = new Vector2(0.9f, -0.3f + delta);
                for (int i = 0; i < 4; i++) {
                    position1.x = position2.x = (position1.x + position2.x) / 2;
//                    position1 = balancer1.reachable(position1, 0.005f);
//                    position2 = balancer2.reachable(position2, 0.005f);
                    position1 = balancer1.reachable(position1);
                    position2 = balancer2.reachable(position2);
                    if (position1.x == position2.x) break;
                }

                //angle = (float)Math.atan2(acc.x, world.getGravity().len());

                //System.out.printf("\n%.3f %.3f", acc1.x, acc1.y);

//                float[] angles = balancer1.calcAngles(position1);
//                Point[] points = balancer1.points();
//                for (int i = 0; i < points.length; i++) {
//                    points[i].setPointer(angles[i]);
//                }
                float intensity = (balancer1.intensity() + balancer2.intensity()) / 2;

                balanceShift1 = new DirectMove(balancer1, position1).setTime(0.01f).start();

//                angles = balancer2.calcAngles(position2);
//                points = balancer2.points();
//                for (int i = 0; i < points.length; i++) {
//                    points[i].setPointer(angles[i]);
//                }
                balanceShift2 = new DirectMove(balancer2, position2).setTime(0.01f).start();

                //System.out.printf("\n%.3f %.3f", balancer1.pointerSpeed().x, balancer1.pointerSpeed().y);
            }
        }

        GL11 gl = Gdx.graphics.getGL11();
        gl.glClearColor(0, 0, 0, 1);
        gl.glClear(GL11.GL_COLOR_BUFFER_BIT);

        Vector3 pos = new Vector3(stickmanBody.centerOfMass.centerOfMass().x, stickmanBody.centerOfMass.centerOfMass().y, 0);
        camera.project(pos);
        //Gdx.input.setCursorPosition((int)pos.x, (int)pos.y);

        renderer.render(world, camera.combined);

        Gdx.graphics.setTitle(String.format("FPS: %d  TOTAL: %.1fMB  USED: %.1fMB",
                Gdx.graphics.getFramesPerSecond(),
                runtime.totalMemory() / 1024f / 1024f,
                (runtime.totalMemory() - runtime.freeMemory()) / 1024f / 1024f));

                //System.out.printf("\r%d", Gdx.graphics.getFramesPerSecond());
//        System.out.printf("\r%6.1f%%%6.1f%%%6.1f%%%6.1f%%",
//                point1.tension() * 100,
//                point2.tension() * 100,
//                point3.tension() * 100,
//                point4.tension() * 100);
                //System.out.printf("\r%.3f %.3f", cm.centerOfMass().x, cm.centerOfMass().y);
                //System.out.printf("\r%.1fkg", cm.totalMass());
//        System.out.printf("\r%.3f %.3f", balancer1.intensity, balancer2.intensity);
//                System.out.printf("\rFPS: %d\t\tTOTAL: %.1fMB USED: %.1fMB",
//                        Gdx.graphics.getFramesPerSecond(),
//                        runtime.totalMemory() / 1024f / 1024f,
//                        (runtime.totalMemory() - runtime.freeMemory()) / 1024f / 1024f);
        //System.out.printf("\r%.2f", totalTorque);
        //System.out.printf("%.3f %.3f\n", balancer1.position().x, balancer1.position().y);
        //System.out.printf("\r%.3f %.3f", balancer1.intensity, balancer2.intensity);
        //System.out.printf("\r%.4f", balancer1.angle() * MathUtils.radDeg);
//        System.out.printf("\r%.4f %.4f %.4f",
//                stickmanBody.centerOfMass.inertia(),
//                stickmanBody.centerOfMass.angularMomentum(),
//                stickmanBody.centerOfMass.angularSpeed() * MathUtils.radDeg);
    }

    @Override
    public void resize(int width, int height) {
        Vector3 prevPos = camera.position.cpy();
        camera.setToOrtho(false, Gdx.graphics.getWidth() / scale, Gdx.graphics.getHeight() / scale);
        camera.position.set(prevPos);
        camera.update();
    }

    @Override
    public void dispose() {
        world.dispose();
    }

    @Override
    public void pause() { }

    @Override
    public void resume() { }

    @Override
    public boolean keyDown(int key) {
        switch (key) {
            case Input.Keys.Q:
                stick1.body().applyLinearImpulse(new Vector2(-5, 0), stick1.A());
                break;
            case Input.Keys.E:
                stick1.body().applyLinearImpulse(new Vector2(+5, 0), stick1.A());
                break;
            case Input.Keys.A:
                stick1.body().applyLinearImpulse(new Vector2(-10, 0), stick1.B());
                break;
            case Input.Keys.D:
                stick1.body().applyLinearImpulse(new Vector2(+10, 0), stick1.B());
                break;
            case Input.Keys.W:
                stick1.body().applyLinearImpulse(new Vector2(0, 50), stick1.A());
                break;
            case Input.Keys.S:
                stick1.body().applyLinearImpulse(new Vector2(0, -50), stick1.B());
                break;
            case Input.Keys.Z:
                staticBody.setLinearVelocity(-1.5f, 0);
                break;
            case Input.Keys.C:
                staticBody.setLinearVelocity(+1.5f, 0);
                break;
            case Input.Keys.SPACE:
                limb1.orientation = limb1.orientation == Limb.Orientation.CW ? Limb.Orientation.CCW : Limb.Orientation.CW;
                limb2.orientation = limb2.orientation == Limb.Orientation.CW ? Limb.Orientation.CCW : Limb.Orientation.CW;
                break;
            case Input.Keys.J:
                delta = 0.2f;
                break;
            case Input.Keys.L:
                delta = -0.2f;
                break;
            case Input.Keys.K:
                delta = 0;
                break;
            case Input.Keys.U:
                angle = 30 * MathUtils.degRad;
                break;
            case Input.Keys.O:
                angle = -30 * MathUtils.degRad;
                break;
            case Input.Keys.I:
                angle = 0;
                break;
            case Input.Keys.ENTER:
                create();
                this.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
                break;
            case Input.Keys.M:
                bendA = new Bend(pointA, 1f).setTime(0.2f).start();
                bendB = new Bend(pointB, 1f).start().synchronize(bendA);
                break;
            case Input.Keys.N:
                bendA = new Bend(pointA, -1f).setTime(0.2f).start();
                bendB = new Bend(pointB, -1f).start().synchronize(bendA);
                break;
            case Input.Keys.V:
                //directMove = new DirectMove(arbBalancer, new Vector2(0.4f, -0.7f)).setTime(10f).start();
                //simpleMove = new SimpleMove(limb, new Vector2(0.4f, -0.7f)).setSpeed(1f).start();
                bendC = new Bend(pointC, +1f).setTime(0.5f).start();
                directMove = new DirectMove(arbBalancer, new Vector2(0.4f, -0.7f)).synchronize(bendC).start();
                //simpleMove = new SimpleMove(limb, new Vector2(0.4f, -0.7f)).synchronize(bendC).start();
                action = new Sequence(new Group(directMove, bendC)).start();
                break;
            case Input.Keys.B:
                //simpleMove = new SimpleMove(limb, new Vector2(0.4f, 0.71f)).setSpeed(1f).start();
                bendC = new Bend(pointC, -1f).setTime(5f).start();
                directMove = new DirectMove(arbBalancer, new Vector2(Float.NaN, 0.7f)).synchronize(bendC).start();
                DirectMove d2 = new DirectMove(arbBalancer, new Vector2(0.2f, Float.NaN)).setTime(3f).start();
                //simpleMove = new SimpleMove(limb, new Vector2(0.4f, 0.71f)).synchronize(bendC).start();
                action = new Sequence(new Group(bendC, d2, directMove)).start();
                break;
            case Input.Keys.X:
                stickmanBrain.stride();
                break;
        }
        return true;
    }

    @Override
    public boolean keyUp(int key) {
        switch (key) {
            case Input.Keys.Z:
            case Input.Keys.C:
                staticBody.setLinearVelocity(0, 0);
        }
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return true;
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {
        Vector3 pos = new Vector3(x, y, 0);
        camera.unproject(pos);
        Vector2 worldPos = new Vector2(pos.x, pos.y);
//        simpleMove1 = new SimpleMove(limb1, limb1.worldToLocal(worldPos)).moveForTime(3f, MathUtils.PI / 1);
//        directMove1 = new DirectMove(limb2, limb2.worldToLocal(worldPos)).moveSynchronously(simpleMove1, 0.2f, 0.7f);
//        balanceShift1 = new DirectMove(limb1, limb1.worldToLocal(worldPos)).moveForTime(1f, MathUtils.PI);
//        balanceShift2 = new DirectMove(limb2, limb2.worldToLocal(worldPos)).moveForTime(1f, MathUtils.PI);
        return true;
    }

    @Override
    public boolean touchUp(int x, int y, int pointer, int button) {
        return true;
    }

    @Override
    public boolean touchDragged(int x, int y, int pointer) {
        return true;
    }

    @Override
    public boolean mouseMoved(int x, int y) {
        Vector3 pos = new Vector3(x, y, 0);
        camera.unproject(pos);
        Vector2 localPos = limb1.worldToLocal(new Vector2(pos.x, pos.y));
        Vector2 worldPos = limb1.localToWorld(localPos);
        //System.out.printf("\r%.2f  %.2f    %.2f  %.2f", localPos.x, localPos.y, worldPos.x, worldPos.y);
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        for (int i = 0; i < Math.abs(amount); i++) {
            if (amount < 0) {
                scale *= 1.1f;
                camSpeed /= 1.1f;
            } else {
                scale /= 1.1f;
                camSpeed *= 1.1f;
            }
        }
        this.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        return true;
    }

    public static void main(String[] args) {
        LwjglApplicationConfiguration conf = new LwjglApplicationConfiguration();
        conf.title = "stickmangine test";
        conf.useGL20 = false;
        conf.vSyncEnabled = false;
        conf.width = 1366;
        conf.height = 768;
        conf.resizable = true;
        conf.samples = 4;
        new LwjglApplication(new Test(), conf);
    }

}
