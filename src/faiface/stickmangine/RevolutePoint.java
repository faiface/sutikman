package faiface.stickmangine;

import com.badlogic.gdx.math.MathUtils;

/**
 * RevolutePoint is an abstraction of anything that can be rotated.
 * RevolutePoint is controlled using "pointer". Pointer is "pointing" to an angle that a RevolutePoint has got to be at.
 * A RevolutePoint is always trying to get to this angle as fast as possible.
 */
public abstract class RevolutePoint {

    /**
     * Sets the angle of pointer in radians.
     * @return itself for easier manipulation
     */
    public abstract RevolutePoint setPointer(float angle);

    /**
     * @return angle of pointer in radians
     */
    public abstract float pointer();

    /**
     * @return actual angle that the RevolutePoint is currently at
     */
    public abstract float angle();

    /**
     * @return difference between actual angle and pointer
     */
    public final float error() {
        return angleDiff(angle(), pointer());
    }

    /**
     * @return absolute value of difference between actual angle and pointer
     */
    public final float absError() {
        return Math.abs(error());
    }

    /**
     * @param alpha angle in radians
     * @param beta angle in radians
     * @return minimal angular rotation from the alpha angle to face the same direction as the beta angle
     */
    public static float angleDiff(float alpha, float beta) {
        float k = (beta - alpha < 0) ? -MathUtils.PI : MathUtils.PI;
        return (beta - alpha + k) % (2 * MathUtils.PI) - k;
    }

}
