package faiface.stickmangine;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

/**
 * Interface for every part of the body of a stick creature
 */
public interface Segment {

    /**
     * @return local coordinates of A point of the segment
     */
    public Vector2 localA();

    /**
     * @return world coordinates of A point of the segment
     */
    public Vector2 A();

    /**
     * Teleports the segment so that it's point A is at the specified location while segment's
     * rotation and size isn't changed
     * @param pos world coordinates
     */
    public void setA(Vector2 pos);

    /**
     * @return local coordinates of A point of the segment
     */
    public Vector2 localB();

    /**
     * @return world coordinates of B point of the segment
     */
    public Vector2 B();

    /**
     * Teleports the segment so that it's point B is at the specified location while segment's
     * rotation and size isn't changed
     * @param pos world coordinates
     */
    public void setB(Vector2 pos);

    /**
     * @return distance between points A and B of the segment
     */
    public float length();

    /**
     * @return angular orientation of the segment in radians
     */
    public float angle();

    /**
     * @return physical body of the segment
     */
    public Body body();

}
