package faiface.stickmangine.balance;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import faiface.stickmangine.MovementPoint;
import faiface.stickmangine.Point;
import faiface.stickmangine.RevolutePoint;

//TODO: remove the fucking acceleration

/**
 * Balancer modifies target position of another MP so that it maintains balance.
 * It adjusts the position against floor and it's friction. You need to provide the friction yourself in
 * form of acceleration (which I found to be more useful, then providing the friction force itself).
 * Balancer also acts as a RevolutePoint. Using it's methods you can rotate and bend body that the MP is
 * connected to while keeping balanced.
 */
public class Balancer extends RevolutePoint implements MovementPoint {

    public CenterOfMass centerOfMass;
    public MovementPoint movementPoint;

    private float correctAngle;
    private float intensity;
    private final Vector2 acc = new Vector2(0, 0);

    private final int DEFAULT_PRECISION = 16;

    /**
     * @param com center of mass that covers whole characters body
     * @param mp movement point that is gonna be used to maintain balance (for example leg)
     * @param correctAngle initial correct angle
     * @param intensity initial intensity
     */
    public Balancer(CenterOfMass com, MovementPoint mp, float correctAngle, float intensity) {
        assert com != null && mp != null : "com or mp not specified";

        centerOfMass = com;
        movementPoint = mp;
        this.correctAngle = correctAngle;
        this.intensity = intensity;
    }

    /**
     * @param cm center of mass that covers whole characters body
     * @param mp movement point that is gonna be used to maintain balance (for example leg)
     */
    public Balancer(CenterOfMass cm, MovementPoint mp) {
        this(cm, mp, 0, 0);
    }

    /**
     * Sets the angle at which the body to which the MP is connected should be relatively to the gravity.
     * @param angle correct angle in radians
     */
    public Balancer setCorrectAngle(float angle) {
        correctAngle = angle;
        return this;
    }

    /**
     * @return correct angle in radians
     */
    public float correctAngle() {
        return correctAngle;
    }

    /**
     * Intensity by the name specifies, how intensively will Balancer try to maintain balance and how intensively it
     * will maintain correct posture.
     * Intensity value 0 means that Balancer will only try to maintain correct posture, while value 1 means that it
     * will only try to maintain balance (so upper body may just slouch). Value in-between will result in
     * compromise.
     * It is recommended to dynamically change this value depending on current situation.
     * @param intensity value between 0 and 1 (out of range value will be clamped)
     */
    public Balancer setIntensity(float intensity) {
        this.intensity = MathUtils.clamp(intensity, 0, 1);
        return this;
    }

    /**
     * @return current intensity
     */
    public float intensity() {
        return intensity;
    }

    /**
     * Sets acceleration that is being applied to the characted by the friction of floor in world coordinates.
     * @param acceleration acceleration in m/s^2
     * @return this instance for easier manipulation
     */
    public Balancer setAcceleration(Vector2 acceleration) {
        acc.set(acceleration);
        return this;
    }

    /**
     * @return current acceleration previously set by setAcceleration()
     */
    public Vector2 acceleration() {
        return acc.cpy();
    }

    /**
     * Position of the MP in Balancer's local space (very transformed). It depends on intensity
     * and acceleration as well.
     * @param intensity usually from 0 to 1
     * @return local space coordinates
     */
    public Vector2 position(float intensity, Vector2 acceleration) {
        return worldToLocal(movementPoint.localToWorld(movementPoint.position()), intensity, acceleration);
    }

    public Vector2 position(float intensity) {
        return position(intensity, acc);
    }

    public Vector2 position(Vector2 acceleration) {
        return position(intensity, acceleration);
    }

    @Override
    public Vector2 position() {
        return position(intensity, acc);
    }

    /**
     * Pointer position of the MP in Balancer's local space (very transformed). It depends on intensity
     * and acceleration as well.
     * @param intensity usually from 0 to 1
     * @param acceleration acceleration of the center of mass in m/s^2
     * @return local space coordinates
     */
    public Vector2 pointerPos(float intensity, Vector2 acceleration) {
        return worldToLocal(movementPoint.localToWorld(movementPoint.pointerPos()), intensity, acceleration);
    }

    public Vector2 pointerPos(float intensity) {
        return pointerPos(intensity, acc);
    }

    public Vector2 pointerPos(Vector2 acceleration) {
        return pointerPos(intensity, acceleration);
    }

    @Override
    public Vector2 pointerPos() {
        return pointerPos(intensity, acc);
    }

    /**
     * Position of the MP in Balancer's local space relative to the center of mass. It depends on intensity
     * and acceleration as well.
     * @param intensity usually from 0 to 1
     * @return local space coordinates
     */
    public Vector2 comPosition(float intensity, Vector2 acceleration) {
        Vector2 comPosition = worldToLocal(centerOfMass.centerOfMass(), intensity, acceleration);
        return position(intensity, acceleration).sub(comPosition);
    }

    public Vector2 comPosition(float intensity) {
        return comPosition(intensity, acc);
    }

    public Vector2 comPosition(Vector2 acceleration) {
        return comPosition(intensity, acceleration);
    }

    public Vector2 comPosition() {
        return comPosition(intensity, acc);
    }

    /**
     * Pointer position of the MP in Balancer's local space relative to the center of mass. It depends on intensity
     * and acceleration as well.
     * @param intensity usually from 0 to 1
     * @param acceleration acceleration of the center of mass in m/s^2
     * @return local space coordinates
     */
    public Vector2 comPointerPos(float intensity, Vector2 acceleration) {
        Vector2 comPosition = worldToLocal(centerOfMass.centerOfMass(), intensity, acceleration);
        return pointerPos(intensity, acceleration).sub(comPosition);
    }

    public Vector2 comPointerPos(float intensity) {
        return comPointerPos(intensity, acc);
    }

    public Vector2 comPointerPos(Vector2 acceleration) {
        return comPointerPos(intensity, acceleration);
    }

    public Vector2 comPointerPos() {
        return comPointerPos(intensity, acc);
    }

    /**
     * @return number of Points of MP
     */
    @Override
    public int numOfPoints() {
        return movementPoint.numOfPoints();
    }

    /**
     * @return array of Points of MP
     */
    @Override
    public Point[] points() {
        return movementPoint.points();
    }

    /**
     * Checks wheter the given position is reachable or not.
     * @param position position in local coordinates
     * @param intensity usually from 0 to 1 (wished to hear more?)
     * @param acceleration acceleration of the center of mass in m/s^2
     * @return true if the given position is reachable, otherwise false
     */
    public boolean isReachable(Vector2 position, float intensity, Vector2 acceleration) {
        if (centerOfMass == null || movementPoint == null) {    // Balancer is not properly set up
            return false;
        }

        Vector2 M = localToWorld(position, intensity, acceleration);
        Vector2 P = movementPoint.worldToLocal(M);
        return movementPoint.isReachable(P);
    }

    public boolean isReachable(Vector2 position, float intensity) {
        return isReachable(position, intensity, acc);
    }

    public boolean isReachable(Vector2 position, Vector2 acceleration) {
        return isReachable(position, intensity, acceleration);
    }

    @Override
    public boolean isReachable(Vector2 position) {
        return isReachable(position, intensity, acc);
    }

    /**
     * Returns given position if it's reachable. If it's not, returns the closest position that still maintains balance.
     * @param position position in local coordinates
     * @param intensity usually from 0 to 1 (wished to hear more?)
     * @param acceleration acceleration of the center of mass in m/s^2
     * @param precision maximum number of iterations to be used to find the correct solution
     * @return reachable position
     */
    public Vector2 reachable(Vector2 position, float intensity, Vector2 acceleration, int precision) {
        if (centerOfMass == null || movementPoint == null) {    // Balancer is not properly set up
            return null;
        }

        Vector2 M = localToWorld(position, intensity, acceleration);
        Vector2 P = movementPoint.worldToLocal(M);
        Vector2 f = localToWorld(new Vector2(1, 0), intensity, acceleration).sub(centerOfMass.centerOfMass()).nor();

        for (int i = 0; i < precision; i++) {
            Vector2 R = movementPoint.reachable(P);
            if (R.equals(P)) {
                break;
            }

            Vector2 worldR = movementPoint.localToWorld(R);
            Vector2 newR = f.cpy().mul(worldR.cpy().sub(M).dot(f)).add(M);
            P.set(movementPoint.worldToLocal(newR));
        }

        return worldToLocal(movementPoint.localToWorld(P), intensity, acceleration);
    }

    // seven fucking "variations" down here; could have been only one if Java supported default parameters

    public Vector2 reachable(Vector2 position, float intensity, Vector2 acceleration) {
        return reachable(position, intensity, acceleration, DEFAULT_PRECISION);
    }

    public Vector2 reachable(Vector2 position, float intensity, int precision) {
        return reachable(position, intensity, acc, precision);
    }

    public Vector2 reachable(Vector2 position, float intensity) {
        return reachable(position, intensity, acc, DEFAULT_PRECISION);
    }

    public Vector2 reachable(Vector2 position, Vector2 acceleration, int precision) {
        return reachable(position, intensity, acceleration, precision);
    }

    public Vector2 reachable(Vector2 position, Vector2 acceleration) {
        return reachable(position, intensity, acceleration, DEFAULT_PRECISION);
    }

    public Vector2 reachable(Vector2 position, int precision) {
        return reachable(position, intensity, acc, precision);
    }

    @Override
    public Vector2 reachable(Vector2 position) {
        return reachable(position, intensity, acc, DEFAULT_PRECISION);
    }

    /**
     * Returns angles that are necessary to be reached by MP's points in order to reach a given target position.
     * @param position target position of the movement point in local coordinates
     * @return correct angles ordered to respectively match Points returned from points() method
     */
    @Override
    public float[] calcAngles(Vector2 position) {
        return movementPoint.calcAngles(movementPoint.worldToLocal(localToWorld(position)));
    }

    /**
     * Coverts a position in local coordinates (very transformed) to a position in world coodinates using
     * secret twists and dark magic. Intensity and acceleration affect results (wished a longer explanation?).
     * @param local local coordinates
     * @param intensity usually from 0 to 1
     * @param acceleration acceleration of the center of mass in m/s^2
     * @return world coordinates
     */
    public Vector2 localToWorld(Vector2 local, float intensity, Vector2 acceleration) {
        if (centerOfMass == null || movementPoint == null) {    // Balancer is not properly set up
            return null;
        }

        Vector2 g, f, P, T;
        Vector2 M = centerOfMass.centerOfMass();
        Vector2 force = acceleration.cpy().mul(centerOfMass.totalMass());

        g = movementPoint.localToWorld(new Vector2(centerOfMass.totalForce().len(), 0)).
                sub(movementPoint.localToWorld(new Vector2(0, 0)));
        g.rotate(-correctAngle * MathUtils.radDeg);
        g.rotate(intensity * angleDiff(g.angle() * MathUtils.degRad, centerOfMass.totalForce().angle() * MathUtils.degRad)
                * MathUtils.radDeg);
        f = g.cpy().sub(force.cpy().sub(g.cpy().nor().mul(force.dot(g.cpy().nor()))));
        P = new Vector2(local.x, 0).rotate(g.angle()).add(movementPoint.localToWorld(new Vector2(0, 0)));
        T = f.cpy().mul((g.dot(P) - g.dot(M)) / g.dot(f)).add(M);
        T.add(new Vector2(0, local.y).rotate(g.angle()));
        return T;
    }

    public Vector2 localToWorld(Vector2 local, float intensity) {
        return localToWorld(local, intensity, acc);
    }

    public Vector2 localToWorld(Vector2 local, Vector2 acceleration) {
        return localToWorld(local, intensity, acceleration);
    }

    @Override
    public Vector2 localToWorld(Vector2 local) {
        return localToWorld(local, intensity, acc);
    }

    /**
     * Coverts a position in world coodinates to a position in local coordinates (very transformed)
     * using dark magic and secret twists. Intensity affects results.
     * @param world world coordinates
     * @param intensity usually from 0 to 1
     * @param acceleration acceleration of the center of mass in m/s^2
     * @return local coordinates
     */
    public Vector2 worldToLocal(Vector2 world, float intensity, Vector2 acceleration) {
        if (centerOfMass == null || movementPoint == null) {    // Balancer is not properly set up
            return null;
        }

        Vector2 g, f, gp;
        Vector2 M = centerOfMass.centerOfMass();
        Vector2 L = movementPoint.localToWorld(new Vector2(0, 0));
        Vector2 force = acceleration.cpy().mul(centerOfMass.totalMass());

        g = movementPoint.localToWorld(new Vector2(centerOfMass.totalForce().len(), 0)).
                sub(movementPoint.localToWorld(new Vector2(0, 0)));
        g.rotate(-correctAngle * MathUtils.radDeg);
        g.rotate(intensity * angleDiff(g.angle() * MathUtils.degRad, centerOfMass.totalForce().angle() * MathUtils.degRad)
                * MathUtils.radDeg);
        gp = new Vector2(-g.y, g.x);
        f = g.cpy().sub(force.cpy().sub(g.cpy().nor().mul(force.dot(g.cpy().nor()))));
        float x = (world.cpy().sub(L)).dot(g.cpy().nor());
        float y = (world.cpy().sub(M).sub(f.cpy().mul((g.dot(world) - g.dot(M)) / g.dot(f)))).dot(gp.cpy().nor());
        return new Vector2(x, y);
    }

    public Vector2 worldToLocal(Vector2 world, float intensity) {
        return worldToLocal(world, intensity, acc);
    }

    public Vector2 worldToLocal(Vector2 world, Vector2 acceleration) {
        return worldToLocal(world, intensity, acceleration);
    }

    @Override
    public Vector2 worldToLocal(Vector2 world) {
        return worldToLocal(world, intensity, acc);
    }

    /**
     * Does nothing.
     * This should be called once a tick.
     * (/r/Jokes/)
     * @param dt delta time in seconds
     * @return this instance for easier manipulation
     */
    @Override
    public Balancer update(float dt) {
        // no-thing
        return this;
    }

    /**
     * Sets correctAngle. Explained above.
     * @param angle angle in radians
     * @return this instance for easier manipulation
     */
    @Override
    public Balancer setPointer(float angle) {
        correctAngle = angle;
        return this;
    }

    /**
     * @return correctAngle
     */
    @Override
    public float pointer() {
        return correctAngle;
    }

    /**
     * @return actual angle of the body to which the MP is connected relatively to the gravity
     */
    @Override
    public float angle() {
        if (centerOfMass == null || movementPoint == null) {    // Balancer is not properly set up
            return Float.NaN;
        }

        float angle = angleDiff(centerOfMass.totalForce().angle() * MathUtils.degRad,
                movementPoint.localToWorld(new Vector2(1, 0)).sub(
                        movementPoint.localToWorld(new Vector2(0, 0))).angle() * MathUtils.degRad);
        return correctAngle + angleDiff(correctAngle, angle);
    }

}
