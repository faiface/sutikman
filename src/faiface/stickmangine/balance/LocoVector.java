package faiface.stickmangine.balance;

/**
 * LocoVector defines a point of a vault. Additionally, it can calculate any next or previous point in time, position
 * or velocity. Let's look at what "the vault" actually is:
 *
 *  O ->
 *   \
 *    \
 *     \
 *      \
 *
 * The point marked with O is the center of mass of a figure. The line aiming to the bottom right from the COM is let's
 * say a leg. The center of mass is located at some position (which is negative because it's on the left), and
 * is moving at some velocity. The velocity is represented by the arrow.
 * Now, where will the center of mass be in 0.5 seconds? Where will it be when it's moving at 0.37m/s? How fast will it
 * be moving at the equilibrium position (0)? The functions below answer (thanks to Wolfram|Alpha) all of these questions,
 * and you can also integrate additional artificial acceleration, which is important when controlling the movement of a
 * figure.
 */
public class LocoVector {

    private float g;    // gravity
    private float a;    // acceleration
    private float h;    // height
    private float p;    // position
    private float v;    // velocity
    private float t;    // time

    /**
     * @param gravity vertical acceleration of gravity in m/s^2
     * @param acceleration additional artificial horizontal acceleration of the center of mass in m/s^2
     * @param height height of the center of mass above the ground in meters
     * @param position horizontal position of center of mass relative to the equilibrium position in meters
     * @param velocity horizontal velocity of the center of mass in m/s
     * @param time point in time of the LocoVector in seconds
     */
    public LocoVector(float gravity, float acceleration, float height, float position, float velocity, float time) {
        set(gravity, acceleration, height, position, velocity, time);
    }

    /**
     * Time is automatically set to 0.
     * @param gravity vertical acceleration of gravity in m/s^2
     * @param acceleration additional artificial horizontal acceleration of the center of mass in m/s^2
     * @param height height of the center of mass above the ground in meters
     * @param position horizontal position of center of mass relative to the equilibrium position in meters
     * @param velocity horizontal velocity of the center of mass in m/s
     */
    public LocoVector(float gravity, float acceleration, float height, float position, float velocity) {
        this(gravity, acceleration, height, position, velocity, 0);
    }

    /**
     * Creates a LocoVector from the current state of a given Balancer.
     * Gravity is obtained from the World attached to the Balancer.
     * Height and position values are taken from the Balancer's position relative to the center of mass.
     * Velocity is calculated from the speed of the center of mass.
     * @param balancer Balancer from which to create a LocoVector
     * @param acceleration additional artificial horizontal acceleration of the center of mass in m/s^2
     * @param time point in time of the LocoVector in seconds
     * @return LocoVector created from the given Balancer
     */
    public static LocoVector fromBalancer(Balancer balancer, float acceleration, float time) {
        float g = balancer.centerOfMass.world().getGravity().len();
        float h = balancer.comPointerPos().x;
        float p = -balancer.comPointerPos().y;
        float v = balancer.centerOfMass.speed().dot(balancer.centerOfMass.world().getGravity().cpy().nor().rotate(90));
        return new LocoVector(g, acceleration, h, p, v, time);
    }

    /**
     * Time is automatically set to 0.
     * @param balancer Balancer from which to create a LocoVector
     * @param acceleration additional artificial horizontal acceleration of the center of mass in m/s^2
     * @return LocoVector created from the given Balancer
     */
    public static LocoVector fromBalancer(Balancer balancer, float acceleration) {
        return fromBalancer(balancer, acceleration, 0);
    }

    /**
     * Time and acceleration are set to 0.
     * @param balancer Balancer from which to create a LocoVector
     * @return LocoVector created from the given Balancer
     */
    public static LocoVector fromBalancer(Balancer balancer) {
        return fromBalancer(balancer, 0, 0);
    }

    /**
     * Assignes new values to this LocoVector (basically an in-place constructor).
     * @param gravity vertical acceleration of gravity in m/s^2
     * @param acceleration additional artificial horizontal acceleration of the center of mass in m/s^2
     * @param height height of the center of mass above the ground in meters
     * @param position horizontal position of center of mass relative to the equilibrium position in meters
     * @param velocity horizontal velocity of the center of mass in m/s
     * @param time point in time of the LocoVector in seconds
     * @return this instance for easier manipulation
     */
    public LocoVector set(float gravity, float acceleration, float height, float position, float velocity, float time) {
        setGravity(gravity);
        setAcceleration(acceleration);
        setHeight(height);
        setPosition(position);
        setVelocity(velocity);
        setTime(time);
        return this;
    }

    /**
     * Copies values from another LocoVector to this LocoVector (basically an in-place copy).
     * @param other LocoVector to be copied
     * @return this instance for easier manipulation
     */
    public LocoVector set(LocoVector other) {
        set(other.gravity(), other.acceleration(), other.height(), other.position(), other.velocity(), other.time());
        return this;
    }

    /**
     * Makes a copy of this LocoVector.
     * @return copy of this LocoVector
     */
    public LocoVector cpy() {
        return new LocoVector(g, a, h, p, v, t);
    }

    /**
     * @return acceleration of gravity in m/s^2
     */
    public float gravity() {
        return g;
    }

    /**
     * @param gravity vertical acceleration of gravity in m/s^2
     * @return this instance for easier manipulation
     */
    public LocoVector setGravity(float gravity) {
        g = Math.abs(gravity);
        return this;
    }

    /**
     * @return additional artificial horizontal acceleration of the center of mass in m/s^2
     */
    public float acceleration() {
        return a;
    }

    /**
     * @param acceleration additional artificial horizontal acceleration of the center of mass in m/s^2
     * @return this instance for easier manipulation
     */
    public LocoVector setAcceleration(float acceleration) {
        a = acceleration;
        return this;
    }

    /**
     * @return height of the center of mass above the ground in meters
     */
    public float height() {
        return h;
    }

    /**
     * @param height height of the center of mass above the ground in meters
     * @return this instance for easier manipulation
     */
    public LocoVector setHeight(float height) {
        h = Math.abs(height);
        return this;
    }

    /**
     * @return horizontal position of center of mass relative to the equilibrium position in meters
     */
    public float position() {
        return p;
    }

    /**
     * @param position horizontal position of center of mass relative to the equilibrium position in meters
     * @return this instance for easier manipulation
     */
    public LocoVector setPosition(float position) {
        p = position;
        return this;
    }

    /**
     * @return horizontal velocity of the center of mass in m/s
     */
    public float velocity() {
        return v;
    }

    /**
     * @param velocity horizontal velocity of the center of mass in m/s
     * @return this instance for easier manipulation
     */
    public LocoVector setVelocity(float velocity) {
        v = velocity;
        return this;
    }

    /**
     * @return point in time of the LocoVector in seconds
     */
    public float time() {
        return t;
    }

    /**
     * @param time point in time of the LocoVector in seconds
     * @return this instance for easier manipulation
     */
    public LocoVector setTime(float time) {
        t = time;
        return this;
    }

    /**
     * Natural acceleration is a horizontal acceleration of the center of mass caused by the position of the
     * center of mass relative to the point touching the ground.
     * @return natural horizontal acceleration of the center of mass
     */
    public float naturalAcceleration() {
        return p * g / h;
    }

    /**
     * @return sum of natural and artificial acceleration of the center of mass
     */
    public float totalAcceleration() {
        return acceleration() + naturalAcceleration();
    }

    /**
     * Calculates a LocoVector of this vault that "happens" at the given time.
     * @param time point of time in seconds
     * @return LocoVector at the requested time
     */
    public LocoVector atTime(float time) {
        double rtime = time - t;    // we have to make it relative to our time, leads to simpler formulas
        double k = Math.sqrt(g / h);
        double position = ((a + k * k * p) * Math.cosh(k * rtime) - a + k * v * Math.sinh(k * rtime)) / (k * k);
        double velocity = (k * (a + k * k * p) * Math.sinh(k * rtime) + k * k * v * Math.cosh(k * rtime)) / (k * k);
        return new LocoVector(g, a, h, (float)position, (float)velocity, time);
    }

    /**
     * Calculates all possible LocoVectors of this vault which possess the given position. There may be zero, one or at
     * most two solutions.
     * @param position horizontal position of center of mass relative to the equilibrium position in meters
     * @return array of LocoVectors with the requested position
     */
    public LocoVector[] atPosition(float position) {
        LocoVector sol1 = null, sol2 = null;

        double k = Math.sqrt(g / h);

        double tsqrt = k * k * (v * v - (p - position) * (2 * a + k * k * (p + position)));
        double tdiv = a + k * (k * p + v);
        if (tsqrt >= 0 && tdiv != 0) {
            // first solution
            double tlog = (a + k * k * position - Math.sqrt(tsqrt)) / tdiv;
            if (tlog > 0) {
                double rtime = Math.log(tlog) / k;
                double velocity = (k * (a + k * k * p) * Math.sinh(k * rtime) + k * k * v * Math.cosh(k * rtime)) / (k * k);
                sol1 = new LocoVector(g, a, h, position, (float)velocity, (float)(t + rtime));
            }

            // second solution
            tlog = (a + k * k * position + Math.sqrt(tsqrt)) / tdiv;
            if (tlog > 0) {
                double rtime = Math.log(tlog) / k;
                double velocity = (k * (a + k * k * p) * Math.sinh(k * rtime) + k * k * v * Math.cosh(k * rtime)) / (k * k);
                sol2 = new LocoVector(g, a, h, position, (float)velocity, (float)(t + rtime));
            }
        }

        if (sol1 == null && sol2 == null) {         // there are no solutions
            return new LocoVector[] { };
        } else if (sol1 == null || sol2 == null) {  // there is only one solution
            return new LocoVector[] { sol2 == null ? sol1 : sol2 };
        } else if (sol1.time() == sol2.time()) {    // there are two solutions, but they're same
            return new LocoVector[] { sol1 };
        } else {                                    // there are two distinct solutions
            if (sol1.time() < sol2.time()) {
                return new LocoVector[] { sol1, sol2 };
            } else {
                return new LocoVector[] { sol2, sol1 };
            }
        }
    }

    /**
     * Calculates all possible LocoVectors of this vault which possess the given velocity. There may be zero, one or at
     * most two solutions.
     * @param velocity horizontal velocity of the center of mass in m/s
     * @return array of LocoVectors with the requested velocity
     */
    public LocoVector[] atVelocity(float velocity) {
        LocoVector sol1 = null, sol2 = null;

        double k = Math.sqrt(g / h);

        double tsqrt = a * a + k * k * (2 * a * p - v * v + velocity * velocity) + k * k * k * k * p * p;
        double tdiv = a + k * k * p + k * v;
        if (tsqrt >= 0 && tdiv != 0) {
            // first solution
            double tlog = (k * velocity - Math.sqrt(tsqrt)) / tdiv;
            if (tlog > 0) {
                double rtime = Math.log(tlog) / k;
                double position = ((a + k * k * p) * Math.cosh(k * rtime) - a + k * v * Math.sinh(k * rtime)) / (k * k);
                sol1 = new LocoVector(g, a, h, (float)position, velocity, (float)(t + rtime));
            }

            // second solution
            tlog = (k * velocity + Math.sqrt(tsqrt)) / tdiv;
            if (tlog > 0) {
                double rtime = Math.log(tlog) / k;
                double position = ((a + k * k * p) * Math.cosh(k * rtime) - a + k * v * Math.sinh(k * rtime)) / (k * k);
                sol2 = new LocoVector(g, a, h, (float)position, velocity, (float)(t + rtime));
            }
        }

        if (sol1 == null && sol2 == null) {         // there are no solutions
            return new LocoVector[] { };
        } else if (sol1 == null || sol2 == null) {  // there is only one solution
            return new LocoVector[] { sol2 == null ? sol1 : sol2 };
        } else if (sol1.time() == sol2.time()) {    // there are two solutions, but they're same
            return new LocoVector[] { sol1 };
        } else {                                    // there are two distinct solutions
            if (sol1.time() < sol2.time()) {
                return new LocoVector[] { sol1, sol2 };
            } else {
                return new LocoVector[] { sol2, sol1 };
            }
        }
    }

}
