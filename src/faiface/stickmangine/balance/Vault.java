package faiface.stickmangine.balance;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import faiface.stickmangine.RevolutePoint;
import faiface.stickmangine.movement.Action;
import faiface.stickmangine.movement.DirectMove;

public class Vault implements Action {

    public final Balancer balancer;

    private CenterOfMass angCom;

    private final LocoVector original, current;
    private LocoVector target;
    private float locoInt, angExp, angInt;
    private float locoSoftness;
    private float[] softness;

    private boolean started = false;

    /**
     * @param def specification of the new Vault
     */
    public Vault(VaultDef def) {
        this.balancer = def.balancer;
        this.angCom = def.angularCenterOfMass;

        if (def.targetPosition != null) {
            setTarget(def.targetPosition, def.targetVelocity);
        }
        original = adaptLocoVector(LocoVector.fromBalancer(balancer));
        current = original.cpy();

        setLocoIntensity(def.locoIntensity);
        setAngularExplosiveness(def.angularExplosiveness);
        setAngularIntensity(def.angularIntensity);
        setLocoSoftness(def.locoSoftness);
        setSoftness(def.softness);
    }

    /**
     * Sets a new target of the Vault.
     * @param targetPosition the position of the center of mass to be reached
     * @param targetVelocity velocity to be reached at the target position
     * @return this instance for easier manipulation
     */
    public Vault setTarget(Vector2 targetPosition, float targetVelocity) {
        if (targetPosition != null) {
            target = new LocoVector(balancer.centerOfMass.world().getGravity().len(), 0,
                                    targetPosition.x, targetPosition.y, targetVelocity);

            // if original has already been set, it's time needs to be updated
            if (original != null) {
                original.set(adaptLocoVector(original));
            }
        } else {
            target = null;
        }
        return this;
    }

    /**
     * @return target position vector
     */
    public Vector2 targetPosition() {
        if (target == null) {
            return null;
        }
        return new Vector2(target.height(), target.position());
    }

    /**
     * @return velocity to be reached at the target position
     */
    public float targetVelocity() {
        if (target == null) {
            return 0;
        }
        return target.velocity();
    }

    /**
     * Changes the center of mas from which the Vault is obtaining information about the angular rotation.
     * This can be done in runtime.
     * @param com desired center of mass
     * @return this instance for easier manipulation
     */
    public Vault setAngularCenterOfMass(CenterOfMass com) {
        assert com != null : "center of mass is null";

        angCom = com;
        return this;
    }

    /**
     * @return current angular center of mass
     */
    public CenterOfMass angularCenterOfMass() {
        return angCom;
    }

    /**
     * @param locoIntensity new loco intensity, absolute value will be used
     * @return this instance for easier manipulation
     */
    public Vault setLocoIntensity(float locoIntensity) {
        this.locoInt = Math.abs(locoIntensity);
        return this;
    }

    /**
     * @return current loco intensity
     */
    public float locoIntensity() {
        return locoInt;
    }

    /**
     * @param angExplosiveness new angular explosiveness, absolute value will be used
     * @return this instance for easier manipulation
     */
    public Vault setAngularExplosiveness(float angExplosiveness) {
        this.angExp = Math.abs(angExplosiveness);
        return this;
    }

    /**
     * @return current angular explosiveness
     */
    public float angularExplosiveness() {
        return angExp;
    }

    /**
     * @param angIntensity new angular intensity, absolute value will be used
     * @return this instance for easier manipulation
     */
    public Vault setAngularIntensity(float angIntensity) {
        this.angInt = Math.abs(angIntensity);
        return this;
    }

    /**
     * @return current angular intensity
     */
    public float angularIntensity() {
        return angInt;
    }

    /**
     * @param locoSoftness new loco softness, absolute value will be used
     * @return this instance for easier manipulation
     */
    public Vault setLocoSoftness(float locoSoftness) {
        this.locoSoftness = Math.abs(locoSoftness);
        return this;
    }

    /**
     * @return current loco softness
     */
    public float locoSoftness() {
        return locoSoftness;
    }

    /**
     * @param softness array of softnesses for each bend respectively; if number of softnesses if lower then
     *                 number of bends, the last softness will be used for the rest of the bends
     * @return this instance for easier manipulation
     */
    public Vault setSoftness(float... softness) {
        this.softness = softness.clone();
        return this;
    }

    /**
     * @return copy of the current softness
     */
    public float[] softness() {
        return softness.clone();
    }

    protected LocoVector adaptLocoVector(LocoVector locoVector) {
        locoVector = locoVector.cpy();

        float a = 0;    // the final acceleration to be applied

        // let's apply the locomotive acceleration
        if (target != null) {
            LocoVector[] targetAtRealLVs = target.atPosition(locoVector.position());

            if (targetAtRealLVs.length > 0) {
                LocoVector targetAtReal;
                if (targetAtRealLVs.length == 1) {
                    targetAtReal = targetAtRealLVs[0];
                } else if (Math.abs(targetAtRealLVs[0].velocity() - locoVector.velocity()) < Math.abs(targetAtRealLVs[1].velocity() - locoVector.velocity())) {
                    targetAtReal = targetAtRealLVs[0];
                } else {
                    targetAtReal = targetAtRealLVs[1];
                }

                a += (targetAtReal.velocity() - locoVector.velocity()) * locoInt;
            }
        }

        // let's correct the body angle
        if (angCom != null) {
            // When the angular center of mass is not right above the main center of mass
            // the effect of applied acceleration is lowered. Therefore we have to calculate
            // how much we have to increase the acceleration to get the wanted effect.
            float gravityAngle = angCom.world().getGravity().angle() * MathUtils.degRad;
            float angComAngle = balancer.centerOfMass.centerOfMass().sub(angCom.centerOfMass()).angle() * MathUtils.degRad;
            float delta = RevolutePoint.angleDiff(gravityAngle, angComAngle);
            a += (balancer.error() * angExp - angCom.angularSpeed()) * angInt / Math.cos(delta);
        }

        locoVector.setAcceleration(a);

        if (target == null) {
            return locoVector;
        }

        // let's check out the time at which we reach the target
        LocoVector[] realAtTargetLVs = locoVector.atPosition(target.position());
        if (realAtTargetLVs.length == 0) {  // the target is unreachable under the current circumstances
            locoVector.setTime(Float.NEGATIVE_INFINITY);
        } else if (realAtTargetLVs.length == 1) {
            locoVector.setTime(target.time() - realAtTargetLVs[0].time() + locoVector.time());
        } else if (Math.abs(realAtTargetLVs[0].velocity() - target.velocity()) < Math.abs(realAtTargetLVs[1].velocity() - target.velocity())) {
            locoVector.setTime(target.time() - realAtTargetLVs[0].time() + locoVector.time());
        } else {
            locoVector.setTime(target.time() - realAtTargetLVs[1].time() + locoVector.time());
        }

        return locoVector;
    }

    /**
     * @return copy of the current LocoVector of the Vault
     */
    public LocoVector currentLocoVector() {
        return current.cpy();
    }

    /**
     * Starts vaulting. You should call this method only at the beginning, otherwise the Vault won't function correctly
     * (see the description of the class).
     * @return this instance for easier manipulation
     */
    @Override
    public Vault start() {
        original.set(adaptLocoVector(LocoVector.fromBalancer(balancer)));
        current.set(original);
        started = true;
        return this;
    }

    /**
     * Stops vaulting. SImple as that.
     * @return this in
     */
    @Override
    public Action stop() {
        started = false;
        return this;
    }

    /**
     * Continues vaulting. This should be called once a tick.
     * @param dt delta time in seconds
     * @return this instance if the Vault has not finished yet, null otherwise
     */
    @Override
    public Vault update(float dt) {
        if (!started) return null;

        // if the locoSoftness limit is reached, update the current LocoVector according to the reality
        LocoVector real = adaptLocoVector(LocoVector.fromBalancer(balancer));
        if (Math.abs(current.velocity() - real.velocity()) > 1f / locoSoftness) {
            current.set(real);
        }

        // update the current LocoVector
        current.set(adaptLocoVector(current));
        if (Float.isFinite(current.time())) {
            current.set(current.atTime(current.time() + dt));
        }

        // move!
        //TODO: propertly set the height of the Vault (should be reaching the target height)
        float height = current.height() + balancer.worldToLocal(balancer.centerOfMass.centerOfMass()).x;
        new DirectMove(balancer, new Vector2(height, -current.position()), softness).setTime(dt).start().update(dt);

        if (fraction() < 1) {
            return this;
        } else {
            started = false;
            return null;
        }
    }

    /**
     * Number from 0 to 1 (usually) indicating which part the Vault is currently at.
     * If the Vault lacks a target, this method will return 0.
     * @return current fraction of the Vault
     */
    @Override
    public float fraction() {
        if (!started) {
            return 1;
        }

        if (target == null) {
            return 0;   // obviously, a Vault never finishes if it lacks a target
        } else {
            return MathUtils.clamp(1 - (target.time() - current.time()) / (target.time() - original.time()), 0, 1);
        }
    }
}
