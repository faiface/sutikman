package faiface.stickmangine.balance;

/**
 * This class contains several functions necessary to successfully perform many locomotion actions like walking,
 * running, etc.. The functions specifically solve problems similar to vaulting over one leg. They calculate
 * time, position and velocity of the vault at any point.
 * Explanation:
 *
 *  O ->
 *   \
 *    \
 *     \
 *      \
 *
 * The point marked with O is the center of mass of a figure. The line aiming to the bottom right from the COM is let's
 * say a leg. The center of mass is located at some position (which is negative because it's on the left), and
 * is moving at some velocity. The velocity is represented by the arrow.
 * Now, where will the center of mass be in 0.5 seconds? Where will it be when it's moving at 0.37m/s? How fast will it
 * be moving at the equilibrium position (0)? The functions below answer (thanks to Wolfram|Alpha) all of these questions,
 * and more (read: but not much more).
 *
 * Warning: This class is now deprecated and will be removed soon. Use the LocoVector class instead which provides
 * same and additional functionality.
 */
@Deprecated
public class Locomotion {

    /**
     * LocoVector defines a moment of a vault by objectifying time, position and velocity.
     */
    public static class LocoVector {

        private final float time, pos, vel;

        /**
         * @param time seconds
         * @param position meters
         * @param velocity meters per second
         */
        public LocoVector(float time, float position, float velocity) {
            this.time = time;
            this.pos = position;
            this.vel = velocity;
        }

        /**
         * @return time in seconds
         */
        public float time() {
            return time;
        }

        /**
         * @return position in meters (relative to the equilibrium position)
         */
        public float position() {
            return pos;
        }

        /**
         * @return velocity in meters per second
         */
        public float velocity() {
            return vel;
        }

        @Override
        public String toString() {
            return "[t:" + time + ",p:"+ pos + ",v:" + vel + "]";
        }

    }

    private Locomotion() { }    // static class

    /**
     * Calculates the LocoVector at a given time from an initial configuration.
     * @param g acceleration of gravity in m/s^2
     * @param h height of the center of mass above the point touching the ground
     * @param p0 initial position (at time 0)
     * @param v0 initial velocity (at time 0)
     * @param t requested time
     * @return LocoVector at the requested time
     */
    public static LocoVector atTime(float g, float h, float p0, float v0, float t) {
        double k = Math.sqrt(g / h);
        double p = p0 * Math.cosh(k * t) + v0 * Math.sinh(k * t) / k;
        double v = k * p0 * Math.sinh(k * t) + v0 * Math.cosh(k * t);
        return new LocoVector(t, (float)p, (float)v);
    }

    /**
     * Calculates the LocoVector at a given position. This may have 0, 1, or 2 solutions.
     * If it has 0 solutions, only an empty array is returned.
     * If it has 1 solution, an array of length 1 is returned.
     * If it has 2 solutions, an array of length 2 is returned with the solution ordered by time.
     * @param g acceleration of gravity in m/s^2
     * @param h height of the center of mass above the point touching the ground
     * @param p0 initial position (at time 0)
     * @param v0 initial velocity (at time 0)
     * @param p requested position
     * @return array of solutions in LocoVectors
     */
    public static LocoVector[] atPosition(float g, float h, float p0, float v0, float p) {
        LocoVector sol1 = null, sol2 = null;

        // using doubles to increse precision of intermediate steps
        double k = Math.sqrt(g / h);

        double tsqrt = -k*k * p0*p0 + k*k * p*p + v0*v0;
        double tdiv = k * p0 + v0;
        if (tsqrt >= 0 && tdiv != 0) {
            // first solution
            double tlog = (k * p - Math.sqrt(tsqrt)) / tdiv;
            if (tlog > 0) {
                double t = Math.log(tlog) / k;
                double v = k * p0 * Math.sinh(k * t) + v0 * Math.cosh(k * t);
                sol1 = new LocoVector((float)t, p, (float)v);
            }

            // second solution
            tlog = (Math.sqrt(tsqrt) + k * p) / tdiv;
            if (tlog > 0) {
                double t = Math.log(tlog) / k;
                double v = k * p0 * Math.sinh(k * t) + v0 * Math.cosh(k * t);
                sol2 = new LocoVector((float)t, p, (float)v);
            }
        }

        if (sol1 == null && sol2 == null) {
            return new LocoVector[] { };
        } else if (sol2 == null) {
            return new LocoVector[] { sol1 };
        } else if (sol1 == null) {
            return new LocoVector[] { sol2 };
        } else {
            if (sol1.time() < sol2.time()) {
                return new LocoVector[] { sol1, sol2 };
            } else if (sol2.time() < sol1.time()) {
                return new LocoVector[] { sol2, sol1 };
            } else {
                return new LocoVector[] { sol1 };
            }
        }
    }

    /**
     * Calculates the LocoVector at a given velocity. This may have 0, 1, or 2 solutions.
     * If it has 0 solutions, only an empty array is returned.
     * If it has 1 solution, an array of length 1 is returned.
     * If it has 2 solutions, an array of length 2 is returned with the solution ordered by time.
     * @param g acceleration of gravity in m/s^2
     * @param h height of the center of mass above the point touching the ground
     * @param p0 initial position (at time 0)
     * @param v0 initial velocity (at time 0)
     * @param v requested velocity
     * @return array of solutions in LocoVectors
     */
    public static LocoVector[] atVelocity(float g, float h, float p0, float v0, float v) {
        LocoVector sol1 = null, sol2 = null;

        double k = Math.sqrt(g / h);

        double tsqrt = k*k * p0*p0 - v0*v0 + v*v;
        double tdiv = k * p0 + v0;
        if (tsqrt >= 0 && tdiv != 0) {
            // first solution
            double tlog = (v - Math.sqrt(tsqrt)) / tdiv;
            if (tlog > 0) {
                double t = Math.log(tlog) / k;
                double p = p0 * Math.cosh(k * t) + v0 * Math.sinh(k * t) / k;
                sol1 = new LocoVector((float)t, (float)p, v);
            }

            // second solution
            tlog = (Math.sqrt(tsqrt) + v) / tdiv;
            if (tlog > 0) {
                double t = Math.log(tlog) / k;
                double p = p0 * Math.cosh(k * t) + v0 * Math.sinh(k * t) / k;
                sol2 = new LocoVector((float)t, (float)p, v);
            }
        }

        if (sol1 == null && sol2 == null) {
            return new LocoVector[] { };
        } else if (sol2 == null) {
            return new LocoVector[] { sol1 };
        } else if (sol1 == null) {
            return new LocoVector[] { sol2 };
        } else {
            if (sol1.time() < sol2.time()) {
                return new LocoVector[] { sol1, sol2 };
            } else if (sol2.time() < sol1.time()) {
                return new LocoVector[] { sol2, sol1 };
            } else {
                return new LocoVector[] { sol1 };
            }
        }
    }

}
