package faiface.stickmangine.balance;

import com.badlogic.gdx.math.Vector2;

/**
 * Specifies all the properties of a new Vault. Fuck, is Vault that complex?
 */
public class VaultDef {

    // the balancer
    public Balancer balancer = null;

    // information about the target
    /**
     * if not specified, the Vault will be targetless, meaning that it will never finish
     */
    public Vector2 targetPosition = null;
    public float targetVelocity = 0;
    public float locoIntensity = 0;

    // information about maintaining correct angle
    /**
     * angularCenterOfMass is an optional center of mass that is most relevant regarding the angular velocity measurement
     */
    public CenterOfMass angularCenterOfMass = null;
    public float angularExplosiveness = 0;
    public float angularIntensity = 0;

    /**
     * locoSoftness means, how much we care about the outer world
     */
    public float locoSoftness = 0;
    public float[] softness = new float[0];

    /**
     * @param softness array of softnesses for each bend respectively; if number of softnesses if lower then
     *                 number of bends, the last softness will be used for the rest of the bends
     * @return this instance for easier manipulation
     */
    public VaultDef setSoftness(float... softness) {
        this.softness = softness.clone();
        return this;
    }

}
