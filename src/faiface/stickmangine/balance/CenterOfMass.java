package faiface.stickmangine.balance;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import faiface.stickmangine.Updatable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Calculates the center of mass taking given outer forces in account and also provides few other useful features.
 */
public class CenterOfMass implements Updatable {

    protected static class Force {
        public final Vector2 point;
        public final Vector2 force;

        public Force(Vector2 point, Vector2 force) {
            this.point = point.cpy();
            this.force = force.cpy();
        }
    }

    private final List<Body> bodies;
    private final List<Force> forces;
    private final Vector2 referenceSpeed = new Vector2();

    private final Vector2 centerOfMass = new Vector2();
    private float totalMass = 0;
    private final Vector2 totalForce = new Vector2();
    private final Vector2 speed = new Vector2();
    private float inertia = 0;
    private float angMomentum = 0;
    private float angSpeed = 0;

    /**
     * @param bodies list of all bodies of a figure
     */
    public CenterOfMass(Body... bodies) {
        this.bodies = new ArrayList<>();
        this.forces = new ArrayList<>();
        addBodies(bodies);
        update(1);
        speed.set(0, 0);
    }

    /**
     * @return world of the first body in the list, null if there are no bodies
     */
    public World world() {
        if (bodies.size() > 0) {
            return bodies.get(0).getWorld();
        } else {
            return null;
        }
    }

    /**
     * Dynamically add bodies in runtime.
     * @param bodies list of bodies to add
     * @return this instance for easier manipulation
     */
    public CenterOfMass addBodies(Body... bodies) {
        Collections.addAll(this.bodies, bodies);
        return this;
    }

    /**
     * Dynamically remove bodies in runtime.
     * @param bodies list of bodies to remove
     * @return this instance for easier manipulation
     */
    public CenterOfMass removeBodies(Body... bodies) {
        for (Body body: bodies) {
            this.bodies.remove(body);
        }
        return this;
    }

    /**
     * Include an additional force acting on the figure when calculating the center of mass.
     * Note that calling update() will nullify all added forces so you need to add all forces in every tick.
     * @param point position where the force is acting
     * @param force direction of the force
     * @return this instance for easier manipulation
     */
    public CenterOfMass addForce(Vector2 point, Vector2 force) {
        forces.add(new Force(point, force));
        return this;
    }

    /**
     * Reference speed is zero speed. Speed returned from speed() method is relative to this speed. So if reference
     * speed is (2, 3) and world speed is (4, 4) than speed relative to reference speed is (2, 1).
     * @param speed speed in m/s
     * @return this instance for easier manipulation
     */
    public CenterOfMass setReferenceSpeed(Vector2 speed) {
        referenceSpeed.set(speed);
        return this;
    }

    /**
     * @return reference speed in m/s
     */
    public Vector2 referenceSpeed() {
        return referenceSpeed.cpy();
    }

    /**
     * Recalculates all neccessary values. Nullifies all information about outer forces, so if you wanna include
     * them in the next tick, you have to add them again. This method needs to be called once a tick.
     * @param dt delta time in seconds
     * @return this instance for easier manipulation
     */
    @Override
    public CenterOfMass update(float dt) {
        if (bodies.size() == 0) {
            totalMass = 0;
            totalForce.set(0, 0);
            centerOfMass.set(0, 0);
            forces.clear();
            return this;
        }

        // calculate total mass
        totalMass = 0;
        for (Body body: bodies) {
            totalMass += body.getMass();
        }

        // calculate total acting force
        totalForce.set(0, 0);
        for (Body body: bodies) {
            totalForce.add(body.getWorld().getGravity().cpy().mul(body.getGravityScale()).mul(body.getMass()));
        }
        for (Force force: forces) {
            totalForce.add(force.force);
        }

        // calculate the center of mass and it's speed taking outer forces in account
        centerOfMass.set(0, 0);
        speed.set(0, 0);
        for (Body body: bodies) {
            float w = body.getWorld().getGravity().cpy().mul(body.getGravityScale()).mul(body.getMass()).dot(totalForce.cpy().nor());
            centerOfMass.add(body.getPosition().cpy().mul(w));
            speed.add(body.getLinearVelocity().cpy().mul(w));
        }
        for (Force force: forces) {
            float w = force.force.dot(totalForce.cpy().nor());
            centerOfMass.add(force.point.cpy().mul(w));
        }
        centerOfMass.div(totalForce.len());
        speed.div(totalForce.len());

        // calculate the angular speed of the center of mass not taking individual rotations in account
        inertia = 0;
        angMomentum = 0;
        for (Body body: bodies) {
            Vector2 comPos = body.getPosition().cpy().sub(centerOfMass);
            float i = body.getInertia() + body.getMass() * comPos.len2();
            float w = body.getLinearVelocity().cpy().sub(speed()).dot(comPos.cpy().nor().rotate(90)) / comPos.len();
            inertia += i;
            angMomentum += i * w;
        }
        angSpeed = angMomentum / inertia;

        // nullify forces
        forces.clear();

        return this;
    }

    /**
     * @return world position of the center of mass of the figure
     */
    public Vector2 centerOfMass() {
        return centerOfMass.cpy();
    }

    /**
     * @return total mass/weight of the figured
     */
    public float totalMass() {
        return totalMass;
    }

    /**
     * @return total force acting on the figure including gravity
     */
    public Vector2 totalForce() {
        return totalForce.cpy();
    }

    /**
     * @return speed relative to the reference speed at which the center of mass is currently moving in m/s
     */
    public Vector2 speed() {
        return speed.cpy().sub(referenceSpeed);
    }

    /**
     * @return absolute (world) speed at which the center of mass is currently moving in m/s
     */
    public Vector2 worldSpeed() {
        return speed.cpy();
    }

    /**
     * @return sum of all bodies' inertia
     */
    public float inertia() {
        return inertia;
    }

    /**
     * @return sum of angular moments of all bodies relative to the center of mass
     */
    public float angularMomentum() {
        return angMomentum;
    }

    /**
     * @return total angular velocity of the center of mass in rad/s
     */
    public float angularSpeed() {
        return angSpeed;
    }

}
