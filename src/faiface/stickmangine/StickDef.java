package faiface.stickmangine;

import com.badlogic.gdx.math.*;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Specifies all the properties of a new segment/s
 */
public class StickDef {

    public World world = null;
    public Object userData = null;
    public final Filter filter = new Filter();
    public float density = 1, friction = 1, restitution = 0;
    public final Vector2 position = new Vector2();
    public float length = 0, width = 1;
    public float angle = 0;

    //FIXME: TEMPORARY
    public boolean dynamic = true;

    /**
     * Checks whether the stick is short enough to be represented just as a circle.
     * @return true if the stick is short enough, false if it's not (if it's a "fully featured" stick)
     */
    public boolean isCircle() {
        return length * width <= 1.19209289550781250000e-7f; // freaking constant?!
    }

    /**
     * @return world coordinates of point A of the stick
     */
    public Vector2 A() {
        return position.cpy().add(new Vector2(-length / 2, 0).rotate(angle * MathUtils.radDeg));
    }

    /**
     * Moves point A to a specified position
     * @param posA world coordinates
     */
    public void setA(Vector2 posA) {
        Vector2 posB = B();
        length = (float)Math.hypot(posB.x - posA.x, posB.y - posA.y);
        angle = (float)Math.atan2(posB.y - posA.y, posB.x - posA.x);
        position.set(posA.add(posB).div(2));
    }

    /**
     * @return world coordinates of point B of the stick
     */
    public Vector2 B() {
        return position.cpy().add(new Vector2(length / 2, 0).rotate(angle * MathUtils.radDeg));
    }

    /**
     * Moves point B to a specified position
     * @param posB world coordinates
     */
    public void setB(Vector2 posB) {
        Vector2 posA = A();
        length = (float)Math.hypot(posA.x - posB.x, posA.y - posB.y);
        angle = (float)Math.atan2(posB.y - posA.y, posB.x - posA.x);
        position.set(posB.add(posA).div(2));
    }

    /**
     * Adjusts density so that the stick will weight given mass.
     * @param mass mass in kilograms
     */
    public void setMass(float mass) {
        float area;
        if (isCircle()) {
            area = MathUtils.PI * width * width / 4;            // just one circle
        } else {
            area = 2 * (MathUtils.PI * width * width / 4) +     // two circles
                    width * length;                             // middle rectangle
        }
        density = mass / area;
    }

}
