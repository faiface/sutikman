package faiface.stickmangine;

/**
 * Anything that should be updated every tick should implement this interface.
 */
public interface Updatable {

    /**
     * @param dt time that has passed since the last call of this method in seconds
     * @return itself (this) for easier manipulation
     */
    public Updatable update(float dt);

}
