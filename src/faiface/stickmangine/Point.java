package faiface.stickmangine;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;

/**
 * Point is a joint that connects and moves segments. But as soon as the word "Joint" is patented by Box2D
 * we are forbidden to call it Joint. So this is Point, ok?
 * Sophisticated movement of a Point can be achieved using classes in package "faiface.stickmangine.movement".
 */
public class Point extends RevolutePoint implements Updatable {

    public final Segment segmentA, segmentB;
    public final PointDef.Anchor anchorA, anchorB;
    public final float strength, explosiveness, stiffness;

    private final RevoluteJoint joint;
    private float torque, pointer;

    /**
     * @param def specify all properties of the point here
     */
    public Point(PointDef def) {
        RevoluteJointDef jointDef = new RevoluteJointDef();
        jointDef.bodyA = def.segmentA.body();
        jointDef.bodyB = def.segmentB.body();
        jointDef.localAnchorA.set(def.anchorA == PointDef.Anchor.A ?
                def.segmentA.localA() : def.segmentA.localB());
        jointDef.localAnchorB.set(def.anchorB == PointDef.Anchor.A ?
                def.segmentB.localA() : def.segmentB.localB());
        jointDef.referenceAngle = 0;
        joint = (RevoluteJoint)jointDef.bodyA.getWorld().createJoint(jointDef);

        segmentA = def.segmentA;
        segmentB = def.segmentB;
        anchorA = def.anchorA;
        anchorB = def.anchorB;
        strength = def.strength;
        explosiveness = def.explosiveness;
        stiffness = def.stiffness;
        pointer = joint.getBodyB().getAngle() - joint.getBodyA().getAngle();
    }

    @Override
    public Point setPointer(float angle) {
        pointer = angle;
        return this;
    }

    @Override
    public float pointer() {
        return pointer;
    }

    /**
     * @return angle between the segments
     */
    @Override
    public float angle() {
        return joint.getJointAngle();
    }

    /**
     * @return world coordinates of the anchor of the point
     */
    public Vector2 position() {
        return joint.getAnchorA().cpy();
    }

    /**
     * @param dt delta time in seconds
     * @return force applied to the second body (B) connected by the joint
     */
    public Vector2 reactionForce(float dt) {
        return joint.getReactionForce(1 / dt);
    }

    /**
     * @return torque that is currently being applied on the connected segments
     */
    public float torque() {
        return torque;
    }

    /**
     * Point executes actions needed to succesfully reach it's pointer.
     * This method should be called every tick.
     * @param dt delta time in seconds
     * @return this instance for easier manipulation
     */
    @Override
    public Point update(float dt) {
        // calculate the torque
        torque = (error() * explosiveness - joint.getJointSpeed()) * stiffness;

        // correct the torque
        torque = MathUtils.clamp(torque, -strength, +strength);

        // apply the torque
        joint.getBodyA().applyTorque(-torque);
        joint.getBodyB().applyTorque(+torque);

        return this;
    }

}
