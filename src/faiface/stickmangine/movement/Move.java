package faiface.stickmangine.movement;

/**
 * Specifies methods shared by all "moving action" classes like Bend, SimpleMove, DirectMove, etc..
 * Generic type U is a "unit" of movement of the move. If the move is two dimensional, then the U should
 * be something like Vector2, if it's one dimensional, then it should be something like Float.
 */
public abstract class Move implements Action {

    protected float time = Float.NEGATIVE_INFINITY, speed = Float.NEGATIVE_INFINITY;
    protected Action otherAction;
    protected float begin, end;

    /**
     * Sets the time that should pass from calling the start() method to the finish of the move.
     * This overrides previous speed setting.
     * @param time time in seconds
     * @return this instance for easier manpulation
     */
    public Move setTime(float time) {
        this.time = Math.abs(time);
        this.speed = Float.NEGATIVE_INFINITY;
        return this;
    }

    /**
     * @return recently set time in seconds
     */
    public float time() {
        return time;
    }

    /**
     * Sets the speed that the MovementPoint should be moving at.
     * This overrides previous time setting.
     * @param speed speed in m/s
     * @return this instance for easier manipulation
     */
    public Move setSpeed(float speed) {
        this.time = Float.NEGATIVE_INFINITY;
        this.speed = Math.abs(speed);
        return this;
    }

    /**
     * @return recently set speed in m/s
     */
    public float speed() {
        return speed;
    }

    /**
     * Synchronizes with another action so that this move starts bending when the other action reaches "begin" fraction
     * and ends when the other actions reaches "end" fraction.
     * @param otherAction an action to synchronize with
     * @param begin a fraction of the other action when this move should start
     * @param end a fraction of the other action when this move should end
     * @return this instance for easier manipulation
     */
    public Move synchronize(Action otherAction, float begin, float end) {
        this.otherAction = otherAction;
        this.begin = begin;
        this.end = end;
        return this;
    }

    /**
     * Synchronizes with another action from it's start (begin=0) to it's end (end=1).
     * @param otherAction an action to synchronize with
     * @return this instance for easier manipulation
     */
    public Move synchronize(Action otherAction) {
        return synchronize(otherAction, 0, 1);
    }

    /**
     * After this call, the move starts moving.
     * @return returns itself (this) for easier manipulation
     */
    @Override
    public abstract Move start();

    /**
     * Update method that should be called every tick.
     * @param dt delta time in seconds
     * @return returns itself (this) if the action has not finished yet, or null if it has
     */
    @Override
    public abstract Move update(float dt);

    /**
     * Fraction is a value between 0 and 1 that says in which part the move is currently in.
     * For exaple, if a Bend should move a Point from angle 10 degrees to 90 degrees and the current
     * angle is 42 degrees, fraction() should return 0.4.
     * @return fraction of the action
     */
    @Override
    public abstract float fraction();

}
