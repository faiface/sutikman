package faiface.stickmangine.movement;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import faiface.stickmangine.MovementPoint;
import faiface.stickmangine.Point;

/**
 * Moves a MovementPoint to a specific position in a specific time, at a specific speed, or synchronously.
 * Simple means that it just sets up some Bends, so the move is not on a straight line.
 */
public class SimpleMove extends Move {

    public final MovementPoint point;

    private final Vector2 original = new Vector2(), target = new Vector2();
    private float[] softness;

    private Bend[] bends;
    private Group group = null;

    /**
     * @param point a MovementPoint to be simply moved
     * @param target position to be reached in local MP's coordinates
     * @param softness array of softnesses for each bend respectively; if number of softnesses if lower then
     *                 number of bends, the last softness will be used for the rest of the bends
     */
    public SimpleMove(MovementPoint point, Vector2 target, float... softness) {
        assert point != null : "point not specified";

        this.point = point;
        setTarget(target);
        setSoftness(softness);

        int numOfPoints = point.numOfPoints();
        bends = new Bend[numOfPoints];
        Point[] points = point.points();
        for (int i = 0; i < numOfPoints; i++) {
            bends[i] = new Bend(points[i]);
        }
    }

    public SimpleMove(MovementPoint point) {
        this(point, new Vector2(0, 0));
    }

    /**
     * Changes the target position of the move. This can be done during runtime.
     * @param target position in local MP's coordinates
     * @return this instance for easier manipulation
     */
    public SimpleMove setTarget(Vector2 target) {
        this.target.set(target);
        return this;
    }

    /**
     * @return current target position
     */
    public Vector2 target() {
        return target.cpy();
    }

    /**
     * Changes softness of the move. This can be done during runtime.
     * @param softness array of softnesses for each bend respectively; if number of softnesses if lower then
     *                 number of bends, the last softness will be used for the rest of the bends
     * @return this instance for easier manipulation
     */
    public SimpleMove setSoftness(float... softness) {
        this.softness = softness.clone();
        return this;
    }

    /**
     * @return a copy of current softness
     */
    public float[] softness() {
        return softness.clone();
    }

    /**
     * Sets the time that should pass from calling the start() method to the finish of the move.
     * This overrides previous speed setting.
     * @param time time in seconds
     * @return this instance for easier manpulation
     */
    @Override
    public SimpleMove setTime(float time) {
        return (SimpleMove)super.setTime(time);
    }

    /**
     * @return recently set time in seconds
     */
    @Override
    public float time() {
        return super.time();
    }

    /**
     * Sets the speed that the MovementPoint should be moving at.
     * This overrides previous time setting.
     * @param speed speed in m/s
     * @return this instance for easier manipulation
     */
    @Override
    public SimpleMove setSpeed(float speed) {
        return (SimpleMove)super.setSpeed(speed);
    }

    /**
     * @return recently set speed in seconds
     */
    @Override
    public float speed() {
        return super.speed();
    }

    /**
     * Synchronizes with another action so that this move starts bending when the other action reaches "begin" fraction
     * and ends when the other actions reaches "end" fraction.
     * @param otherAction an action to synchronize with
     * @param begin a fraction of the other action when this move should start
     * @param end a fraction of the other action when this move should end
     * @return this instance for easier manipulation
     */
    @Override
    public SimpleMove synchronize(Action otherAction, float begin, float end) {
        return (SimpleMove)super.synchronize(otherAction, begin, end);
    }

    /**
     * Synchronizes with another action from it's start (begin=0) to it's end (end=1).
     * @param otherAction an action to synchronize with
     * @return this instance for easier manipulation
     */
    @Override
    public SimpleMove synchronize(Action otherAction) {
        return (SimpleMove)super.synchronize(otherAction);
    }

    /**
     * Initiates movement of the MovementPoint.
     * @return this instance for easier manipulation
     */
    @Override
    public SimpleMove start() {
        original.set(point.pointerPos());
        group = new Group(bends).start();
        return this;
    }

    /**
     * Stops moving the MovementPoint.
     * @return this instance for easier manipulation
     */
    @Override
    public SimpleMove stop() {
        if (group != null) {
            group.stop();
        }
        group = null;
        return this;
    }

    private void prepareBends() {
        for (Bend bend: bends) {
            bend.setTime(time);
            if (otherAction != null) {
                bend.synchronize(otherAction, begin, end);
            }
        }
    }

    /**
     * Continues moving the MovementPoint. If the move has not started, this method will not do anything.
     * This method needs to be called every tick.
     * @param dt delta time in seconds
     * @return this instance if the move has not finished yet, null if it has
     */
    @Override
    public SimpleMove update(float dt) {
        if (group == null) return null;

        // change speed according to recent calls of setTime, setSpeed or synchronize methods
        if (time == Float.NEGATIVE_INFINITY && speed == Float.NEGATIVE_INFINITY) {
            // we are synchronized
            if (otherAction != null) {
                speed = Float.POSITIVE_INFINITY;
                time = 0;
                prepareBends();
            }
            // nothing is set up, nothing to do
            else {
                return null;
            }
        }
        // time is set up
        else if (time != Float.NEGATIVE_INFINITY && speed == Float.NEGATIVE_INFINITY) {
            if (time == 0) {
                speed = Float.POSITIVE_INFINITY;
            } else {
                speed = target.cpy().sub(original).len() / time;
            }
            prepareBends();
        }
        // speed is set up
        else if (time == Float.NEGATIVE_INFINITY && speed != Float.NEGATIVE_INFINITY) {
            if (speed == 0) {
                time = Float.POSITIVE_INFINITY;
            } else {
                time = target.cpy().sub(original).len() / speed;
            }
            prepareBends();
        }

        // change position and softness according to recent changes
        float[] angles = point.calcAngles(target);
        for (int i = 0; i < bends.length; i++) {
            bends[i].setTarget(angles[i]);
            if (softness.length > 0) {
                bends[i].setSoftness(softness[MathUtils.clamp(i, 0, softness.length - 1)]);
            } else {
                bends[i].setSoftness(0);
            }
        }

        // let them shine!
        group.update(dt);

        if (fraction() < 1) {
            return this;
        } else {
            group = null;
            return null;
        }
    }

    /**
     * Number from 0 to 1 indicating which part the move is currently at.
     * @return current fraction of the move
     */
    @Override
    public float fraction() {
        if (group == null) return 1;
        return group.fraction();
    }

}
