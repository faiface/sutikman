package faiface.stickmangine.movement;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import faiface.stickmangine.Point;
import faiface.stickmangine.PointDef;
import faiface.stickmangine.Segment;
import faiface.stickmangine.MovementPoint;

/**
 * Limb is a movement point located at the end of a limb. Limb is composed of three segment and two points.
 * root seg -- a point -- middle seg -- b point -- end seg
 * The limb's orientation specifies whether the end segment of a limb should be rotated clockwise or counterclockwise.
 */
public class Limb implements MovementPoint {

    /**
     * Simple enum for clockwise and counterclockwise orientation.
     */
    public static enum Orientation {
        CW, CCW     // clockwise, counterclockwise
    }

    /**
     * You're allowed to freely change this value in runtime.
     */
    public Orientation orientation;

    private final Point pointA, pointB;
    private Segment root, middle, end;
    private Vector2 rootAnchor, endAnchor;

    private boolean aReversed, aSwapped, bReversed, bSwapped;

    public Limb(Point pointA, Point pointB, Orientation orientation) {
        this.pointA = pointA;
        this.pointB = pointB;
        this.orientation = orientation;

        bReversed = (pointB.anchorA == pointB.anchorB);

        // not really sure if this shit down here is fully correct; it works at least for the most common cases
        if (pointA.segmentA == pointB.segmentA) {
            root = pointA.segmentB;
            rootAnchor = (pointA.anchorB == PointDef.Anchor.A) ? root.localA() : root.localB();
            middle = pointA.segmentA;
            end = pointB.segmentB;
            endAnchor = (pointB.anchorB == PointDef.Anchor.A) ? end.localB() : end.localA();
            aReversed = (pointA.anchorA == PointDef.Anchor.B);
            aSwapped = true;
            bSwapped = false;
        } else if (pointA.segmentA == pointB.segmentB) {
            root = pointA.segmentB;
            rootAnchor = (pointA.anchorB == PointDef.Anchor.A) ? root.localA() : root.localB();
            middle = pointA.segmentA;
            end = pointB.segmentA;
            endAnchor = (pointB.anchorA == PointDef.Anchor.A) ? end.localB() : end.localA();
            aReversed = (pointA.anchorA == PointDef.Anchor.B);
            aSwapped = true;
            bSwapped = true;
        } else if (pointA.segmentB == pointB.segmentA) {
            root = pointA.segmentA;
            rootAnchor = (pointA.anchorA == PointDef.Anchor.A) ? root.localA() : root.localB();
            middle = pointA.segmentB;
            end = pointB.segmentB;
            endAnchor = (pointB.anchorB == PointDef.Anchor.A) ? end.localB() : end.localA();
            aReversed = (pointA.anchorB == PointDef.Anchor.B);
            aSwapped = false;
            bSwapped = false;
        } else if (pointA.segmentB == pointB.segmentB) {
            root = pointA.segmentA;
            rootAnchor = (pointA.anchorA == PointDef.Anchor.A) ? root.localA() : root.localB();
            middle = pointA.segmentB;
            end = pointB.segmentA;
            endAnchor = (pointB.anchorA == PointDef.Anchor.A) ? end.localB() : end.localA();
            aReversed = (pointA.anchorB == PointDef.Anchor.B);
            aSwapped = false;
            bSwapped = true;
        } else {
            assert false : "pointA and pointB do not share any segment";
        }
    }

    /**
     * @return local coordinates of the position of the limb
     */
    @Override
    public Vector2 position() {
        return worldToLocal(end.body().getWorldPoint(endAnchor));
    }

    /**
     * @return local coordinates of an ideal position if all of the Points would point to their pointers
     */
    @Override
    public Vector2 pointerPos() {
        float angleA = pointA.pointer(), angleB = pointB.pointer();

        if (aSwapped) angleA *= -1;
        if (aReversed) angleA -= MathUtils.PI;
        if (bSwapped) angleB *= -1;
        if (bReversed) angleB -= MathUtils.PI;

        Vector2 pointer = new Vector2();
        pointer.add(middle.length(), 0).rotate(angleA * MathUtils.radDeg);
        pointer.add(new Vector2(end.length(), 0).rotate((angleA + angleB) * MathUtils.radDeg));
        return pointer;
    }

    /**
     * Limb is constructed from two points.
     * @return 2
     */
    @Override
    public int numOfPoints() {
        return 2;
    }

    /**
     * @return point A, point B
     */
    @Override
    public Point[] points() {
        return new Point[] { pointA, pointB };
    }

    /**
     * Checks wheter the given position is reachable by the Limb.
     * @param position position in local coordinates
     * @return true if the given position is reachable, otherwise false
     */
    @Override
    public boolean isReachable(Vector2 position) {
        float a = middle.length(),
              b = end.length(),
              d = position.len();
        return d > Math.abs(a - b) && d < a + b;
    }

    /**
     * Returs given position if it is reachable. If it's not it returns the nearest rechable position to the given position.
     * @param position position in local coordinates
     * @return reachable position
     */
    @Override
    public Vector2 reachable(Vector2 position) {
        float a = middle.length(),
              b = end.length(),
              d = position.len();
        if (d >= a + b) {                                // too far
            return position.cpy().mul((a + b) / d);
        } else if (d <= Math.abs(a - b)) {               // too close
            return position.cpy().mul(Math.abs(a - b) / d);
        } else {
            return position.cpy();                      // perfectly reachable
        }
    }

    /**
     * Calculates angles of the controlling Points that are neccessary to be reached
     * in order to move the limb to a given position.
     * @param position target position of the movement point in local coordinates
     * @return correct angles ordered to respectively match Points returned from points() method
     */
    @Override
    public float[] calcAngles(Vector2 position) {
        // calcuate neccessary values
        float angle = (float)Math.atan2(position.y, position.x),
              a = middle.length(),
              b = end.length(),
              d = position.len();

        int k = (orientation == Orientation.CW) ? +1 : -1;

        float[] angles = new float[2];

        // calculate angles
        // we do need to check reachability here because reachable() function isn't all that precise
        if (d >= a + b) {                    // too far
            angles[0] = angle;
            angles[1] = 0;
        } else if (d <= Math.abs(a - b)) {   // too close
            angles[0] = angle + MathUtils.PI;
            angles[1] = MathUtils.PI;
        } else {                            // perfectly reachable
            angles[0] = angle + k * (float)Math.acos((d*d + a*a - b*b) / (2*d*a));
            angles[1] = k * (float)Math.acos((a*a + b*b - d*d) / (2*a*b)) - MathUtils.PI;
        }

        // adjust angles to the segment's relative orientation
        if (aSwapped) angles[0] *= -1;
        if (aReversed) angles[0] += MathUtils.PI;
        if (bSwapped) angles[1] *= -1;
        if (bReversed) angles[1] += MathUtils.PI;

        return angles;
    }

    /**
     * Converts a position in local Limb's coordinates to corresponding world coordinates.
     * @param local local coordinates
     * @return world coordinates
     */
    @Override
    public Vector2 localToWorld(Vector2 local) {
        local = local.cpy();
        local.add(rootAnchor);
        return root.body().getWorldPoint(local).cpy();
    }

    /**
     * Converts a position in world coordinates to corresponding local Limb's coordinates.
     * @param world world coordinates
     * @return local coordinates
     */
    @Override
    public Vector2 worldToLocal(Vector2 world) {
        world = world.cpy();
        Vector2 local = root.body().getLocalPoint(world).cpy();
        return local.sub(rootAnchor);
    }

    /**
     * Does nothing.
     * This should be called once a tick.
     * (/r/Jokes/)
     * @param dt delta time in seconds
     * @return this instance for easier manipulation
     */
    @Override
    public Limb update(float dt) {
        // no-thing
        return this;
    }

}
