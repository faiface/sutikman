package faiface.stickmangine.movement;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import faiface.stickmangine.MovementPoint;
import faiface.stickmangine.Point;

/**
 * Moves a MovementPoint to a specific position in a specific time, at a specific speed, or synchronously.
 * Direct means that the move is executed on a linear path (shortest path from A to B).
 * This move also has a special ability to ignore one of the axes of the MovementPoint, so that you can use two
 * DirectMove's on one point at once (for whatever reason).
 */
public class DirectMove extends Move {

    public final MovementPoint point;

    private final Vector2 original = new Vector2(), target = new Vector2();
    private float[] softness;

    private Bend[] bends;
    private boolean started = false;

    private boolean ignoreX, ignoreY;

    /**
     * @param point a MovementPoint to be directly moved
     * @param target position to be reached in local MP's coordinates
     * @param softness array of softnesses for each bend respectively; if number of softnesses if lower then
     *                 number of bends, the last softness will be used for the rest of the bends
     */
    public DirectMove(MovementPoint point, Vector2 target, float... softness) {
        assert point != null : "point not specified";

        this.point = point;
        setTarget(target);
        setSoftness(softness);

        int numOfPoints = point.numOfPoints();
        bends = new Bend[numOfPoints];
        Point[] points = point.points();
        for (int i = 0; i < numOfPoints; i++) {
            bends[i] = new Bend(points[i]);
            bends[i].setSpeed(Float.POSITIVE_INFINITY);
        }
    }

    public DirectMove(MovementPoint point) {
        this(point, new Vector2(0, 0));
    }

    // this method is ay m-fucking ignorant
    private void ignoreAxes() {
        if (ignoreX) {
            original.x = point.pointerPos().x;
            target.x = point.pointerPos().x;
        }
        if (ignoreY) {
            original.y = point.pointerPos().y;
            target.y = point.pointerPos().y;
        }
    }

    /**
     * Changes the target position of the move. This can be done during runtime.
     * If one of the axis of the target is set to NaN, that axis will be ignored (not taken care of).
     * @param target position in local MP's coordinates
     * @return this instance for easier manipulation
     */
    public DirectMove setTarget(Vector2 target) {
        ignoreX = Float.isNaN(target.x);
        ignoreY = Float.isNaN(target.y);
        ignoreAxes();
        this.target.set(target);
        return this;
    }

    /**
     * @return current target position
     */
    public Vector2 target() {
        return target.cpy();
    }

    /**
     * Changes softness of the move. This can be done during runtime.
     * @param softness array of softnesses for each bend respectively; if number of softnesses if lower then
     *                 number of bends, the last softness will be used for the rest of the bends
     * @return this instance for easier manipulation
     */
    public DirectMove setSoftness(float... softness) {
        this.softness = softness.clone();
        return this;
    }

    /**
     * @return a copy of current softness
     */
    public float[] softness() {
        return softness.clone();
    }

    /**
     * Sets the time that should pass from calling the start() method to the finish of the move.
     * This overrides previous speed setting.
     * @param time time in seconds
     * @return this instance for easier manpulation
     */
    @Override
    public DirectMove setTime(float time) {
        return (DirectMove)super.setTime(time);
    }

    /**
     * @return recently set time in seconds
     */
    @Override
    public float time() {
        return super.time();
    }

    /**
     * Sets the speed that the MovementPoint should be moving at.
     * This overrides previous time setting.
     * @param speed speed in m/s
     * @return this instance for easier manipulation
     */
    @Override
    public DirectMove setSpeed(float speed) {
        return (DirectMove)super.setSpeed(speed);
    }

    /**
     * @return recently set speed in seconds
     */
    @Override
    public float speed() {
        return super.speed();
    }

    /**
     * Synchronizes with another action so that this move starts bending when the other action reaches "begin" fraction
     * and ends when the other actions reaches "end" fraction.
     * @param otherAction an action to synchronize with
     * @param begin a fraction of the other action when this move should start
     * @param end a fraction of the other action when this move should end
     * @return this instance for easier manipulation
     */
    @Override
    public DirectMove synchronize(Action otherAction, float begin, float end) {
        return (DirectMove)super.synchronize(otherAction, begin, end);
    }

    /**
     * Synchronizes with another action from it's start (begin=0) to it's end (end=1).
     * @param otherAction an action to synchronize with
     * @return this instance for easier manipulation
     */
    @Override
    public DirectMove synchronize(Action otherAction) {
        return (DirectMove)super.synchronize(otherAction);
    }

    /**
     * Initiates movement of the MovementPoint.
     * @return this instance for easier manipulation
     */
    @Override
    public DirectMove start() {
        original.set(point.pointerPos());
        started = true;
        return this;
    }

    /**
     * Stops movement of the MovementPoint.
     * @return this instance for easier manipulation
     */
    @Override
    public DirectMove stop() {
        started = false;
        return this;
    }

    /**
     * Continues moving the MovementPoint. If the move has not started, this method will not do anything.
     * This method needs to be called every tick.
     * @param dt delta time in seconds
     * @return this instance if the move has not finished yet, null if it has
     */
    @Override
    public DirectMove update(float dt) {
        if (!started) return null;

        // ignore the NaN axes
        ignoreAxes();

        // change speed according to recent calls of setTime, setSpeed or synchronize methods
        if (time == Float.NEGATIVE_INFINITY && speed == Float.NEGATIVE_INFINITY) {
            // we are synchronized
            if (otherAction != null) {
                speed = Float.POSITIVE_INFINITY;
                time = 0;
            }
            // nothing is set up, nothing to do
            else {
                return null;
            }
        }
        // time is set up
        else if (time != Float.NEGATIVE_INFINITY && speed == Float.NEGATIVE_INFINITY) {
            if (time == 0) {
                speed = Float.POSITIVE_INFINITY;
            } else {
                speed = target.cpy().sub(original).len() / time;
            }
        }
        // speed is set up
        else if (time == Float.NEGATIVE_INFINITY && speed != Float.NEGATIVE_INFINITY) {
            if (speed == 0) {
                time = Float.POSITIVE_INFINITY;
            } else {
                time = target.cpy().sub(original).len() / speed;
            }
        }

        // synchronize if we're synchronizing
        Vector2 targetPos = target.cpy();
        if (otherAction != null) {
            float fraction = (otherAction.fraction() - begin) / (end - begin);
            if (fraction > 0 && fraction < 1) {
                targetPos.set(original.cpy().add(target.cpy().sub(original).mul(fraction)));
            } else {    // we're out of the scope
                targetPos.set(point.pointerPos());
            }
        }

        // adjust the target position to our speed
        Vector2 d = targetPos.cpy().sub(point.pointerPos());
        d.set(d.cpy().nor().mul(MathUtils.clamp(speed * dt, 0, d.len())));
        if (target.dst(point.pointerPos()) > d.len()) {
            targetPos.set(point.pointerPos().add(d));
        } else {
            targetPos.set(target);
            started = false;
        }

        // moving
        float[] angles = point.calcAngles(targetPos);
        for (int i = 0; i < bends.length; i++) {
            bends[i].setTarget(angles[i]);
            if (softness.length > 0) {
                bends[i].setSoftness(softness[MathUtils.clamp(i, 0, softness.length - 1)]);
            } else {
                bends[i].setSoftness(0);
            }
            bends[i].start().update(dt);
        }

        if (fraction() < 1) {
            return this;
        } else {
            started = false;
            return null;
        }
    }

    /**
     * Number from 0 to 1 indicating which part the move is currently at.
     * @return current fraction of the move
     */
    @Override
    public float fraction() {
        if (!started) return 1;
        else {
            ignoreAxes();
            Vector2 direction = target.cpy().sub(original);
            return point.pointerPos().sub(original).dot(direction.cpy().nor()) / direction.len();
        }
    }

}
