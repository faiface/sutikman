package faiface.stickmangine.movement;

import faiface.stickmangine.Updatable;

/**
 * Every class implementing Action interface can be used in a Sequence or a Group.
 */
public interface Action extends Updatable {

    /**
     * After this call, the action starts acting.
     * @return returns itself (this) for easier manipulation
     */
    public Action start();

    /**
     * After this call, the action stops acting.
     * @return returns itself (this) for easier manipulation
     */
    public Action stop();

    /**
     * Update method that should be called every tick.
     * @param dt delta time in seconds
     * @return returns itself (this) if the action has not finished yet, or null if it has
     */
    @Override
    public Action update(float dt);

    /**
     * Fraction is a value between 0 and 1 that says in which part the action is currently in.
     * For exaple, if a Bend should move a Point from angle 10 degrees to 90 degrees and the current
     * angle is 42 degrees, fraction() should return 0.4.
     * Implementing this method is important for synchronization.
     * @return current fraction of the action
     */
    public float fraction();

}
