package faiface.stickmangine.movement;

/**
 * Group is an Action that executes multiple other actions simultaneously.
 */
public class Group implements Action {

    /**
     * Number of actions.
     */
    public final int length;

    private final Action[] actions;
    private final boolean[] finished;
    private int done;

    public Group(Action... actions) {
        this.actions = actions.clone();
        length = actions.length;
        finished = new boolean[length];
        done = 0;
    }

    /**
     * Starts all actions.
     * @return this instance for easier manipulation
     */
    @Override
    public Group start() {
        done = 0;
        for (int i = 0; i < length; i++) {
            if (actions[i] != null) {
                actions[i].start();
                finished[i] = false;
            } else {
                finished[i] = true;
                done++;
            }
        }
        return this;
    }

    /**
     * Stops all the actions and halts the group.
     * @return this instance for easier manipulation
     */
    @Override
    public Group stop() {
        for (int i = 0; i < length; i++) {
            if (actions[i] != null) {
                actions[i].stop();
            }
            finished[i] = true;
        }
        done = length;
        return this;
    }

    /**
     * Updates all actions.
     * @param dt delta time in seconds
     * @return this instance if the actions have not finished yet, null if they have
     */
    @Override
    public Group update(float dt) {
        for (int i = 0; i < length; i++) {
            if (!finished[i] && actions[i].update(dt) == null) {
                finished[i] = true;
                done++;
            }
        }
        if (done < length) return this;
        else return null;
    }

    /**
     * Fraction of the group is the lowest fraction from all the Actions.
     * @return fraction of the group
     */
    @Override
    public float fraction() {
        float min = 1;
        for (Action action: actions) {
            if (action != null) {
                min = Math.min(min, action.fraction());
            }
        }
        return min;
        // down here is the former code that calculated the average of the fractions but was causing issues with synchronization
//        float sum = 0;
//        for (Action action: actions) {
//            if (action != null) {
//                sum += action.fraction();
//            } else {
//                sum += 1;
//            }
//        }
//        return sum / length;
    }

}
