package faiface.stickmangine.movement;

/**
 * Sequence is an Action that executes other actions one after another.
 */
public class Sequence implements Action {

    /**
     * Number of actions.
     */
    public final int length;

    private final Action[] actions;
    private int current;

    public Sequence(Action... actions) {
        this.actions = actions.clone();
        length = actions.length;
        current = length;
    }

    /**
     * Starts the sequence from the first action.
     * @return this instance for easier manipulation
     */
    @Override
    public Sequence start() {
        current = 0;
        actions[current].start();
        return this;
    }

    /**
     * Stops executing the sequence. It also stops the currently running Action.
     * @return this instance for easier manipulation
     */
    @Override
    public Sequence stop() {
        if (current < length) {
            actions[current].stop();
        }
        current = length;
        return this;
    }

    /**
     * Continues execution of all the actions one after another.
     * @param dt delta time in seconds
     * @return this instance if the sequnce has not finished yet, null if it has
     */
    @Override
    public Sequence update(float dt) {
        if (current >= length) {                            // already finished
            return null;
        } else if (actions[current].fraction() < 1) {       // continue executing the current action
            actions[current].update(dt);
        } else if (++current < length) {                    // start executing a next action
            actions[current].start().update(dt);
        } else {                                            // just finished
            return null;
        }
        return this;
    }

    /**
     * Fraction of the sequence is combined. Best explanation is by examples:
     * 2 actions, 0.25 fraction = half of the first action
     * 4 actions, 0.25 fraction = first action just finished
     * 3 actions, 0.44 fraction = third of the second action
     * @return fraction of the sequence
     */
    @Override
    public float fraction() {
        if (current >= length) {
            return 1;
        }
        return (1f / actions.length * (current + actions[current].fraction()));
    }

}
