package faiface.stickmangine.movement;

import com.badlogic.gdx.math.MathUtils;
import faiface.stickmangine.Point;
import faiface.stickmangine.RevolutePoint;

/**
 * Bend is an Action used to bend a RevolutePoint from it's current angle to another angle for a given time, at a given speed,
 * and optionaly synchonously with another Action.
 */
public class Bend extends Move {

    public final RevolutePoint point;

    private float original, target;
    private float softness;

    private boolean started = false;

    /**
     * @param point a RevolutePoint to be bent
     * @param target an angle that should be reached in radians
     * @param softness level of adaptation to outer forces
     */
    public Bend(RevolutePoint point, float target, float softness) {
        assert point != null : "point not specified";

        this.point = point;
        setTarget(target);
        setSoftness(softness);
    }

    public Bend(Point point, float target) {
        this(point, target, 0);
    }

    public Bend(Point point) {
        this(point, 0);
    }

    /**
     * Changes target angle. You can do this even if the bend is currently running. It will not change the speed
     * of the bend.
     * @param angle angle in radians
     * @return this instance for easier manipulation
     */
    public Bend setTarget(float angle) {
        this.target = angle;
        return this;
    }

    /**
     * @return current target angle in radians
     */
    public float target() {
        return target;
    }

    /**
     * Changes level of adaptation to outer forces. Softness besically means the maximum error of the RevolutePoint
     * ran through this formula softness=PI/max_error.
     * @param softness level of adaptation to outer forces
     * @return this instance for easier manipulation
     */
    public Bend setSoftness(float softness) {
        this.softness = Math.abs(softness);
        return this;
    }

    /**
     * @return current level of adaptation to outer forces
     */
    public float softness() {
        return softness;
    }

    /**
     * Sets the time that should pass from calling the start() method to the finish of the bend.
     * This overrides previous speed setting.
     * @param time time in seconds
     * @return this instance for easier manpulation
     */
    @Override
    public Bend setTime(float time) {
        return (Bend)super.setTime(time);
    }

    /**
     * @return recently set time in seconds
     */
    @Override
    public float time() {
        return super.time();
    }

    /**
     * Sets the speed that the RevolutePoint should be bending at.
     * This overrides previous time setting.
     * @param speed speed in rad/s
     * @return this instance for easier manipulation
     */
    @Override
    public Bend setSpeed(float speed) {
        return (Bend)super.setSpeed(speed);
    }

    /**
     * @return recently set speed in seconds
     */
    @Override
    public float speed() {
        return super.speed();
    }

    /**
     * Synchronizes with another action so that this bend starts bending when the other action reaches "begin" fraction
     * and ends when the other actions reaches "end" fraction.
     * @param otherAction an action to synchronize with
     * @param begin a fraction of the other action when this bend should start
     * @param end a fraction of the other action when this bend should end
     * @return this instance for easier manipulation
     */
    @Override
    public Bend synchronize(Action otherAction, float begin, float end) {
        return (Bend)super.synchronize(otherAction, begin, end);
    }

    /**
     * Synchronizes with another action from it's start (begin=0) to it's end (end=1).
     * @param otherAction an action to synchronize with
     * @return this instance for easier manipulation
     */
    @Override
    public Bend synchronize(Action otherAction) {
        return (Bend)super.synchronize(otherAction);
    }

    /**
     * Initiates bending of the RevolutePoint.
     * @return this instance for easier manipulation
     */
    @Override
    public Bend start() {
        original = point.pointer();
        started = true;
        return this;
    }

    /**
     * Stops bending the RevolutePoint.
     * @return this instance for easier manipulation
     */
    @Override
    public Bend stop() {
        started = false;
        return this;
    }

    /**
     * Continues bending the RevolutePoint. If the bend has not started, this method will not do anything.
     * This method needs to be called every tick.
     * @param dt delta time in seconds
     * @return this instance if the bend has not finished yet, null if it has
     */
    @Override
    public Bend update(float dt) {
        if (!started) return null;

        // change speed according to recent calls of setTime, setSpeed or synchronize methods
        if (time == Float.NEGATIVE_INFINITY && speed == Float.NEGATIVE_INFINITY) {
            // we are synchronized
            if (otherAction != null) {
                speed = Float.POSITIVE_INFINITY;
                time = 0;
            }
            // nothing is set up, nothing to do
            else {
                return null;
            }
        }
        // time is set up
        else if (time != Float.NEGATIVE_INFINITY && speed == Float.NEGATIVE_INFINITY) {
            if (time == 0) {
                speed = Float.POSITIVE_INFINITY;
            } else {
                speed = Math.abs(RevolutePoint.angleDiff(original, target) / time);
            }
        }
        // speed is set up
        else if (time == Float.NEGATIVE_INFINITY && speed != Float.NEGATIVE_INFINITY) {
            if (speed == 0) {
                time = Float.POSITIVE_INFINITY;
            } else {
                time = Math.abs(RevolutePoint.angleDiff(original, target) / speed);
            }
        }

        // synchonize if we're synchronized
        float targetAngle = target;
        if (otherAction != null) {
            float fraction = (otherAction.fraction() - begin) / (end - begin);
            if (fraction > 0 && fraction < 1) {
                targetAngle = original + RevolutePoint.angleDiff(original, target) * fraction;
            } else {    // we're out of the scope
                targetAngle = point.pointer();
            }
        }

        // bending
        float diff = RevolutePoint.angleDiff(point.pointer(), targetAngle);
        if (Math.abs(diff) < speed * dt) {                                      // we're right there!
            point.setPointer(targetAngle);
        } else if (diff > 0) {                                                  // we're ahead
            point.setPointer((point.pointer() + speed * dt));
        } else {                                                                // we're behind
            point.setPointer((point.pointer() - speed * dt));
        }

        // softening
        float angle = point.pointer() - point.error();
        point.setPointer(MathUtils.clamp(point.pointer(), angle - MathUtils.PI/softness, angle + MathUtils.PI/softness));

        if (fraction() < 1) {
            return this;
        } else {
            started = false;
            return null;
        }
    }

    /**
     * Number from 0 to 1 indicating which part the bend is currently at.
     * @return current fraction of the bend
     */
    @Override
    public float fraction() {
        if (!started) return 1;
        return RevolutePoint.angleDiff(original, point.pointer()) / RevolutePoint.angleDiff(original, target);
    }

}
