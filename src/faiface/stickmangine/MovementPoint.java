package faiface.stickmangine;

import com.badlogic.gdx.math.Vector2;
import faiface.stickmangine.Point;

/**
 * MovementPoint defines a way to move a point (not Point) on a figure to a specific position using some Points.
 * MP can be for example an end of a hand, a foot, or anything that can be precisely moved to a specific position.
 */
public interface MovementPoint extends Updatable {

    /**
     * @return local coordinates of the position of the movement point
     */
    public Vector2 position();

    /**
     * @return local coordinates of an ideal position if all of the Points would point to their pointers
     */
    public Vector2 pointerPos();

    /**
     * @return number of Points that the movement point is controlled with
     */
    public int numOfPoints();

    /**
     * @return array of the Points that the movement point is controlled with
     */
    public Point[] points();

    /**
     * Checks wheter the given position is reachable by the MovementPoint and returns the result.
     * @param position position in local coordinates
     * @return true if the given position is reachable, otherwise false
     */
    public boolean isReachable(Vector2 position);

    /**
     * Returs given position if it is reachable. If it's not it returns the nearest rechable position to the given position.
     * @param position position in local coordinates
     * @return reachable position
     */
    public Vector2 reachable(Vector2 position);

    /**
     * Calculates angles of controlling Points that are neccessary to reach in order to move the MP to a given position.
     * @param position target position of the movement point in local coordinates
     * @return correct angles ordered to respectively match Points returned from points() method
     */
    public float[] calcAngles(Vector2 position);

    /**
     * Converts a position in local MP's coordinates to corresponding world coordinates.
     * @param local local coordinates
     * @return world coordinates
     */
    public Vector2 localToWorld(Vector2 local);

    /**
     * Converts a position in world coordinates to corresponding local MP's coordinates.
     * @param world world coordinates
     * @return local coordinates
     */
    public Vector2 worldToLocal(Vector2 world);

    /**
     * Anything that only needs to be done once in a tick can be done here.
     * @param dt delta time in seconds
     * @return this instance for easier manipulation
     */
    @Override
    public MovementPoint update(float dt);

}
