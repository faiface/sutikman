package faiface.stickmangine;

/**
 * Specifies all properties the properties for a new point/s
 */
public class PointDef {

    public static enum Anchor { A, B }

    public Segment segmentA, segmentB;
    public Anchor anchorA = Anchor.B, anchorB = Anchor.A;

    public float strength = 0, explosiveness = 0, stiffness = 0;
}
