package faiface.stickmangine.figures.stickman;

//TODO: implementation
//TODO: documentation
//TODO: test out

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import faiface.stickmangine.Updatable;
import faiface.stickmangine.balance.LocoVector;
import faiface.stickmangine.balance.Vault;
import faiface.stickmangine.balance.VaultDef;
import faiface.stickmangine.movement.DirectMove;

/**
 * StickmanBrain is what "powers" a StickmanBody, it is it's operating system. First of all it makes sure it stands
 * upright and last but not least it executes all of requested activities like walking, jumping, etc.
 */
public class StickmanBrain implements Updatable {

    private final StickmanBody stickmanBody;

    // actions
    private final Stand stand;
    private Stride stride;
    private Vault leftVault, rightVault;

    /**
     * @param stickmanBody the body that we will operate on, it's unchangable
     */
    public StickmanBrain(StickmanBody stickmanBody) {
        this.stickmanBody = stickmanBody;

        stand = new Stand(stickmanBody, 0.4f, 0.92f).start();

        new DirectMove(stickmanBody.balancerA, new Vector2(0.7f, -0.001f)).setTime(0).start().update(1);
        new DirectMove(stickmanBody.balancerB, new Vector2(0.7f, -0.001f)).setTime(0).start().update(1);

        VaultDef vaultDef;

        vaultDef = new VaultDef();
        vaultDef.balancer = stickmanBody.leftBalancer();
//        vaultDef.targetPosition = new Vector2(0.7f, -0.4f);
//        vaultDef.targetVelocity = -2f;
        vaultDef.locoIntensity = 10;
        vaultDef.angularCenterOfMass = stickmanBody.upperCenterOfMass;
        vaultDef.angularExplosiveness = 1;
        vaultDef.angularIntensity = 10;
        vaultDef.locoSoftness = 100;
        leftVault = new Vault(vaultDef).start();

        vaultDef = new VaultDef();
        vaultDef.balancer = stickmanBody.rightBalancer();
//        vaultDef.targetPosition = new Vector2(0.7f, -0.4f);
//        vaultDef.targetVelocity = -2f;
        vaultDef.locoIntensity = 10;
        vaultDef.angularCenterOfMass = stickmanBody.upperCenterOfMass;
        vaultDef.angularExplosiveness = 1;
        vaultDef.angularIntensity = 10;
        vaultDef.locoSoftness = 100;
        rightVault = new Vault(vaultDef).start();
    }

    /**
     * @return the body given in the constructor
     */
    public StickmanBody stickmanBody() {
        return stickmanBody;
    }

    public StickmanBrain stride() {
        stride = new Stride(stickmanBody, new Vector2(0, 0.5f), 0.9f, 1.0f).start();
        return this;
    }

    /**
     * Does all the shit described in the class documentation.
     */
    @Override
    public StickmanBrain update(float dt) {
        if (stride != null) {
            System.out.println(stride.fraction());
            stride = stride.update(dt);
        } else {
            stand.update(dt);
        }

//        stickmanBody.leftBalancer().setIntensity(1);
//        stickmanBody.rightBalancer().setIntensity(1);
//
//        leftVault.setTarget(new Vector2(0.7f, 0.5f), 0.7f);
//        rightVault.setTarget(new Vector2(0.7f, 0.5f), 0.7f);
//
//        leftVault.update(dt);
//        rightVault.update(dt);

        return this;
    }
}
