package faiface.stickmangine.figures.stickman;

//TODO: implementation
//TODO: documentation
//TODO: test out

import com.badlogic.gdx.math.Vector2;
import faiface.stickmangine.balance.Balancer;
import faiface.stickmangine.balance.LocoVector;
import faiface.stickmangine.balance.Vault;
import faiface.stickmangine.balance.VaultDef;
import faiface.stickmangine.movement.Action;
import faiface.stickmangine.movement.DirectMove;
import faiface.stickmangine.movement.Group;
import faiface.stickmangine.movement.Sequence;

public class Stride implements Action {


    protected class Leaning implements Action {

        private Vault lean;
        private DirectMove shift;   // shifting the front leg forwards, maybe we could change the name later
        private Group stretch;
        private boolean started = false;

        public Leaning() {
            VaultDef leanDef = new VaultDef();
            leanDef.balancer = rear;
            leanDef.angularCenterOfMass = stickmanBody.upperCenterOfMass;
            leanDef.angularExplosiveness = 1;
            leanDef.angularIntensity = 1;
            leanDef.locoSoftness = 10;
            lean = new Vault(leanDef);

            shift = new DirectMove(front).synchronize(this);
            stretch = new Group(
                    shift,
                    new Sequence(
                            new DirectMove(front, new Vector2(height - 0.02f, Float.NaN)).setTime(0.01f),
                            new DirectMove(front, new Vector2(height - 0.1f, Float.NaN)).synchronize(this, 0.0f, 0.3f),
                            new DirectMove(front, new Vector2(height, Float.NaN)).synchronize(this, 0.3f, 1.0f)
                    )
            );
        }

        protected void updateLeanTarget() {
            // purpose of this function is to set the target of the lean
            // so that it gives us a reasonable fraction and finishes at
            // the point where we have enough velocity to successfully
            // perform the next part of the stride
            LocoVector current = lean.currentLocoVector();
            LocoVector target = new LocoVector(current.gravity(), 0, height, 0, speed * Math.signum(delta.y));
            float targetVelocity = target.atPosition(current.position() - delta.y)[0].velocity();
            LocoVector[] atVelLVs = current.atVelocity(targetVelocity);
            if (atVelLVs.length > 0) {
                float targetPosition = atVelLVs[atVelLVs.length - 1].position();
                lean.setTarget(new Vector2(height, targetPosition), targetVelocity);
            }
        }

        @Override
        public Leaning start() {
            updateLeanTarget();

            // update the shift's target
            float targetY = front.worldToLocal(rear.localToWorld(rear.position())).y + delta.y;
            shift.setTarget(new Vector2(Float.NaN, targetY));

            lean.start();
            stretch.start();
            started = true;
            return this;
        }

        @Override
        public Leaning stop() {
            lean.stop();
            stretch.stop();
            started = false;
            return this;
        }

        @Override
        public Leaning update(float dt) {
            if (!started) return null;

            updateLeanTarget();

            lean.update(dt);
            stretch.update(dt);

            if (fraction() < 1) {
                return this;
            } else {
                started = false;
                return null;
            }
        }

        @Override
        public float fraction() {
            return lean.fraction();
        }

    }


    private final StickmanBody stickmanBody;
    private final Balancer rear, front;
    private final Vector2 delta = new Vector2();
    private float height;
    private float speed;

    private Sequence stride = null;

    public Stride(StickmanBody stickmanBody, Vector2 delta, float height, float speed) {
        this.stickmanBody = stickmanBody;
        this.rear = delta.y < 0 ? stickmanBody.rightBalancer() : stickmanBody.leftBalancer();
        this.front = delta.y < 0 ? stickmanBody.leftBalancer() : stickmanBody.rightBalancer();
        this.delta.set(delta);
        this.height = height;
        this.speed = Math.abs(speed);
    }

    @Override
    public Stride start() {
        VaultDef vaultDef = new VaultDef();
        vaultDef.balancer = front;
        vaultDef.targetPosition = new Vector2(height, 0.0f);
        vaultDef.targetVelocity = speed * Math.signum(delta.y);
        vaultDef.locoIntensity = 2;
        vaultDef.angularCenterOfMass = stickmanBody.upperCenterOfMass;
        vaultDef.angularExplosiveness = 1;
        vaultDef.angularIntensity = 1;
        Vault vault = new Vault(vaultDef);  //FIXME: find a better name for this

        stride = new Sequence(
                new Leaning(),
                new Group(
                        vault,
                        new Sequence(
                                new DirectMove(rear, new Vector2(height - 0.02f, Float.NaN)).setTime(0.01f),
                                new DirectMove(rear, new Vector2(height - 0.1f, Float.NaN)).synchronize(vault, 0.0f, 0.5f)
                        ),
                        new DirectMove(rear, new Vector2(Float.NaN, 0.0f)).synchronize(vault)
                )
        ).start();
        return this;
    }

    @Override
    public Stride stop() {
        if (stride != null) {
            stride.stop();
        }
        stride = null;
        return this;
    }

    @Override
    public Stride update(float dt) {
        if (stride == null) {
            return null;
        }

        stride = stride.update(dt);
        return this;
    }

    @Override
    public float fraction() {
        if (stride == null) {
            return 1;
        }
        return stride.fraction();
    }

}
