package faiface.stickmangine.figures.stickman;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import faiface.stickmangine.*;
import faiface.stickmangine.balance.Balancer;
import faiface.stickmangine.balance.CenterOfMass;
import faiface.stickmangine.movement.Limb;

import java.util.ArrayList;
import java.util.List;

/**
 * StickmanBody creates all of the components of a stickman obeying given StickmanDef. It provides sensors and
 * movement abstractions like Point and MovementPoint, together creating the I/O (input and output) for the figure.
 * These shall be sensibly utilized using StickmanBrain.
 */
public class StickmanBody implements Updatable {

    // world
    public final World world;

    // body parts
    public final Stick head, throat, breast, stomach;        // torso
    //public final Stick armA, armB, forearmA, forearmB;       // arms
    //public final Stick palmA, palmB;                         // palms
    public final Stick thighA, thighB, shinA, shinB;         // legs

    // centers of mass
    public final CenterOfMass centerOfMass;
    public final CenterOfMass upperCenterOfMass;
    public final CenterOfMass lowerCenterOfMass;

    // joints
    public final Point brain, neck, spine;                   // torso
    //public final Point humerusA, humerusB, elbowA, elbowB;   // arms
    //public final Point wristA, wristB;                       // palms
    public final Point hipA, hipB, kneeA, kneeB;             // legs

    // limbs
    //public final Limb handA, handB;                          // hands
    public final Limb legA, legB;                            // legs

    // balancers
    public final Balancer balancerA, balancerB;              // biped

    // some handy lists
    private final List<Updatable> updatables;

    /**
     * Construct a new StickmanBody.
     * @param def specify all the attributes of the new StickmanBody here
     */
    public StickmanBody(StickmanDef def) {
        this.world = def.world;
        updatables = new ArrayList<>();

        /* construct body parts (some hardcoding, which is fine) */

        StickDef stickDef = new StickDef();
        assert def.world != null : "world is not specified";
        stickDef.world = def.world;
        stickDef.friction = def.friction;
        stickDef.restitution = def.restitution;
        stickDef.width = def.width;
        stickDef.filter.groupIndex = def.index;

        // legs

        stickDef.setB(def.transform.mul(new Vector2(0, 0)));
        stickDef.setA(def.transform.mul(new Vector2(-def.height * 0.25f, 0)));
        stickDef.setMass(def.weight * 0.057f);
        shinA = new Stick(stickDef);
        shinB = new Stick(stickDef);

        stickDef.setB(stickDef.A());
        stickDef.setA(def.transform.mul(new Vector2(-def.height * 0.5f, 0)));
        stickDef.setMass(def.weight * 0.1416f);
        thighA = new Stick(stickDef);
        thighB = new Stick(stickDef);

        // torso

        //stickDef.dynamic = false;
        stickDef.setB(stickDef.A());
        stickDef.setA(def.transform.mul(new Vector2(-def.height * 0.666666667f, 0)));
        //stickDef.setMass(def.weight * 0.175f);    // this will be used once the hands are present
        stickDef.setMass(def.weight * (0.2173f + 0.0271f));
        stomach = new Stick(stickDef);
        //stomach.body().setFixedRotation(true);

        stickDef.setB(stickDef.A());
        stickDef.setA(def.transform.mul(new Vector2(-def.height * 0.833333333f, 0)));
        //stickDef.setMass(def.weight * 0.175f);    // this will be used once the hands are present
        stickDef.setMass(def.weight * 0.2173f + 0.0223f);
        breast = new Stick(stickDef);

        stickDef.setB(stickDef.A());
        stickDef.setA(def.transform.mul(new Vector2(-def.height * 0.916666668f, 0)));
        stickDef.setMass(def.weight * 0.0347f);
        throat = new Stick(stickDef);

        stickDef.setB(stickDef.A());
        stickDef.angle = def.transform.getRotation();
        stickDef.width = def.height * 0.16666667f;
        stickDef.setMass(def.weight * 0.0347f);
        head = new Stick(stickDef);

        /* set up center of mass */
        centerOfMass = new CenterOfMass(head.body(), throat.body(), breast.body(), stomach.body(),
                thighA.body(), thighB.body(), shinA.body(), shinB.body());
        upperCenterOfMass = new CenterOfMass(head.body(), throat.body(), breast.body(), stomach.body());
        lowerCenterOfMass = new CenterOfMass(thighA.body(), thighB.body(), shinA.body(), shinB.body());
        updatables.add(centerOfMass);
        updatables.add(upperCenterOfMass);
        updatables.add(lowerCenterOfMass);

        /* connect body parts */

        //final float STRENGTH_K = def.weight * def.height * def.height * def.strength;
        final float STRENGTH_K = def.strength;
        final float BRAIN_STRENGTH = 0.3f * STRENGTH_K;
        final float NECK_STRENGTH = 0.6f * STRENGTH_K;
        final float SPINE_STRENGTH = 1f * STRENGTH_K;
        final float HIP_STRENGTH = 1.5f * STRENGTH_K;
        final float KNEE_STRENGTH = 1.5f * STRENGTH_K;

        final float EXPLOSIVENESS_K = def.explosiveness;
        final float BRAIN_EXPLOSIVENESS = 0;
        final float NECK_EXPLOSIVENESS = 4 * EXPLOSIVENESS_K;
        final float SPINE_EXPLOSIVENESS = 5 * EXPLOSIVENESS_K;
        final float HIP_EXPLOSIVENESS = 7 * EXPLOSIVENESS_K;
        final float KNEE_EXPLOSIVENESS = 7 * EXPLOSIVENESS_K;

        final float BRAIN_RESTING = 1f;
        final float NECK_RESTING = 0.2f;
        final float SPINE_RESTING = 0.7f;
        final float HIP_RESTING = 0.3f;
        final float KNEE_RESTING = 0.2f;

        //final float STIFFNESS_K = def.weight * (def.width * def.width + def.height * def.height) * def.stiffness;
        final float STIFFNESS_K = def.stiffness;
        final float BRAIN_STIFFNESS = 0.04f * STIFFNESS_K;
        final float NECK_STIFFNESS = 0.08f * STIFFNESS_K;
        final float SPINE_STIFFNESS = 0.16f * STIFFNESS_K;
        final float HIP_STIFFNESS = 0.2f * STIFFNESS_K;
        final float KNEE_STIFFNESS = 0.2f * STIFFNESS_K;

        PointDef pointDef = new PointDef();
        pointDef.explosiveness = def.explosiveness;

        // torso

        pointDef.segmentA = head;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = throat;
        pointDef.anchorB = PointDef.Anchor.A;
        pointDef.strength = BRAIN_STRENGTH;
        //pointDef.explosiveness = BRAIN_EXPLOSIVENESS;
        //pointDef.restingTension = BRAIN_RESTING;
        pointDef.stiffness = BRAIN_STIFFNESS;
        brain = new Point(pointDef);
        updatables.add(brain);

        pointDef.segmentA = throat;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = breast;
        pointDef.anchorB = PointDef.Anchor.A;
        pointDef.strength = NECK_STRENGTH;
        //pointDef.explosiveness = NECK_EXPLOSIVENESS;
        //pointDef.restingTension = NECK_RESTING;
        pointDef.stiffness = NECK_STIFFNESS;
        neck = new Point(pointDef);
        updatables.add(neck);

        pointDef.segmentA = breast;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = stomach;
        pointDef.anchorB = PointDef.Anchor.A;
        pointDef.strength = SPINE_STRENGTH;
        //pointDef.explosiveness = SPINE_EXPLOSIVENESS;
        //pointDef.restingTension = SPINE_RESTING;
        pointDef.stiffness = SPINE_STIFFNESS;
        spine = new Point(pointDef);
        updatables.add(spine);

        // legs

        pointDef.segmentA = stomach;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = thighA;
        pointDef.anchorB = PointDef.Anchor.A;
        pointDef.strength = HIP_STRENGTH;
        //pointDef.explosiveness = HIP_EXPLOSIVENESS;
        //pointDef.restingTension = HIP_RESTING;
        pointDef.stiffness = HIP_STIFFNESS;
        hipA = new Point(pointDef);
        updatables.add(hipA);

        pointDef.segmentA = stomach;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = thighB;
        pointDef.anchorB = PointDef.Anchor.A;
        pointDef.strength = HIP_STRENGTH;
        //pointDef.explosiveness = HIP_EXPLOSIVENESS;
        //pointDef.restingTension = HIP_RESTING;
        pointDef.stiffness = HIP_STIFFNESS;
        hipB = new Point(pointDef);
        updatables.add(hipB);

        pointDef.segmentA = thighA;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = shinA;
        pointDef.anchorB = PointDef.Anchor.A;
        pointDef.strength = KNEE_STRENGTH;
        //pointDef.explosiveness = KNEE_EXPLOSIVENESS;
        //pointDef.restingTension = KNEE_RESTING;
        pointDef.stiffness = KNEE_STIFFNESS;
        kneeA = new Point(pointDef);
        updatables.add(kneeA);

        pointDef.segmentA = thighB;
        pointDef.anchorA = PointDef.Anchor.B;
        pointDef.segmentB = shinB;
        pointDef.anchorB = PointDef.Anchor.A;
        pointDef.strength = KNEE_STRENGTH;
        //pointDef.explosiveness = KNEE_EXPLOSIVENESS;
        //pointDef.restingTension = KNEE_RESTING;
        pointDef.stiffness = KNEE_STIFFNESS;
        kneeB = new Point(pointDef);
        updatables.add(kneeB);

        /* set up limbs and balancers */
        //TODO: let orientation be set up in StickmanDef
        legA = new Limb(hipA, kneeA, Limb.Orientation.CW);
        updatables.add(legA);
        legB = new Limb(hipB, kneeB, Limb.Orientation.CW);
        updatables.add(legB);
        balancerA = new Balancer(centerOfMass, legA);
        updatables.add(balancerA);
        balancerB = new Balancer(centerOfMass, legB);
        updatables.add(balancerB);
    }

    //TODO: test out these methods down here

    /**
     * @param intensity parameter for the Balancer.position() method
     * @param acceleration parameter for the Balancer.position() method
     * @return returns the balancer that is more to the left based on the position obtained using given parameters
     */
    public Balancer leftBalancer(float intensity, Vector2 acceleration) {
        if (balancerA.position(intensity, acceleration).y <= balancerB.position(intensity, acceleration).y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @param intensity parameter for the Balancer.position() method
     * @return returns the balancer that is more to the left based on the position obtained using given parameters
     */
    public Balancer leftBalancer(float intensity) {
        if (balancerA.position(intensity).y <= balancerB.position(intensity).y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @param acceleration parameter for the Balancer.position() method
     * @return returns the balancer that is more to the left based on the position obtained using given parameters
     */
    public Balancer leftBalancer(Vector2 acceleration) {
        if (balancerA.position(acceleration).y <= balancerB.position(acceleration).y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @return returns the balancer that is more to the left based on the position obtained using given parameters
     */
    public Balancer leftBalancer() {
        if (balancerA.position().y <= balancerB.position().y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @param intensity parameter for the Balancer.position() method
     * @param acceleration parameter for the Balancer.position() method
     * @return returns the balancer that is more to the right based on the position obtained using given parameters
     */
    public Balancer rightBalancer(float intensity, Vector2 acceleration) {
        if (balancerA.position(intensity, acceleration).y > balancerB.position(intensity, acceleration).y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @param intensity parameter for the Balancer.position() method
     * @return returns the balancer that is more to the right based on the position obtained using given parameters
     */
    public Balancer rightBalancer(float intensity) {
        if (balancerA.position(intensity).y > balancerB.position(intensity).y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @param acceleration parameter for the Balancer.position() method
     * @return returns the balancer that is more to the right based on the position obtained using given parameters
     */
    public Balancer rightBalancer(Vector2 acceleration) {
        if (balancerA.position(acceleration).y > balancerB.position(acceleration).y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @return returns the balancer that is more to the right based on the position obtained using given parameters
     */
    public Balancer rightBalancer() {
        if (balancerA.position().y > balancerB.position().y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @param intensity parameter for the Balancer.position() method
     * @param acceleration parameter for the Balancer.position() method
     * @return returns the balancer that is more to the left based on the pointer position obtained using given parameters
     */
    public Balancer ptrLeftBalancer(float intensity, Vector2 acceleration) {
        if (balancerA.pointerPos(intensity, acceleration).y < balancerB.pointerPos(intensity, acceleration).y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @param intensity parameter for the Balancer.position() method
     * @return returns the balancer that is more to the left based on the pointer position obtained using given parameters
     */
    public Balancer ptrLeftBalancer(float intensity) {
        if (balancerA.pointerPos(intensity).y < balancerB.pointerPos(intensity).y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @param acceleration parameter for the Balancer.position() method
     * @return returns the balancer that is more to the left based on the pointer position obtained using given parameters
     */
    public Balancer ptrLeftBalancer(Vector2 acceleration) {
        if (balancerA.pointerPos(acceleration).y < balancerB.pointerPos(acceleration).y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @return returns the balancer that is more to the left based on the pointer position obtained using given parameters
     */
    public Balancer ptrLeftBalancer() {
        if (balancerA.pointerPos().y < balancerB.pointerPos().y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @param intensity parameter for the Balancer.position() method
     * @param acceleration parameter for the Balancer.position() method
     * @return returns the balancer that is more to the right based on the pointer position obtained using given parameters
     */
    public Balancer ptrRightBalancer(float intensity, Vector2 acceleration) {
        if (balancerA.pointerPos(intensity, acceleration).y > balancerB.pointerPos(intensity, acceleration).y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @param intensity parameter for the Balancer.position() method
     * @return returns the balancer that is more to the right based on the pointer position obtained using given parameters
     */
    public Balancer ptrRightBalancer(float intensity) {
        if (balancerA.pointerPos(intensity).y > balancerB.pointerPos(intensity).y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @param acceleration parameter for the Balancer.position() method
     * @return returns the balancer that is more to the right based on the pointer position obtained using given parameters
     */
    public Balancer ptrRightBalancer(Vector2 acceleration) {
        if (balancerA.pointerPos(acceleration).y > balancerB.pointerPos(acceleration).y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @return returns the balancer that is more to the right based on the pointer position obtained using given parameters
     */
    public Balancer ptrRightBalancer() {
        if (balancerA.pointerPos().y > balancerB.pointerPos().y) {
            return balancerA;
        } else {
            return balancerB;
        }
    }

    /**
     * @return array of all updatables in the stickman's body
     */
    public Updatable[] updatables() {
        return updatables.toArray(new Updatable[updatables.size()]);
    }

    /**
     * Updates all of the updatables in the body, such as Points, CenterOfMass, etc.
     * @param dt time that has passed since the last call of this method in seconds
     * @return this instance for easier manipulation
     */
    @Override
    public StickmanBody update(float dt) {
        for (Updatable u: updatables) {
            u.update(dt);
        }
        return this;
    }

}
