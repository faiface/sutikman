package faiface.stickmangine.figures.stickman;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import faiface.stickmangine.balance.LocoVector;
import faiface.stickmangine.movement.Action;
import faiface.stickmangine.movement.DirectMove;

/**
 * Standing Action. Always stands, never ends.
 * Standing is a position based locomotion actions, hence it uses features like "intensity", quick moves, etc.
 */
class Stand implements Action {

    private final StickmanBody stickmanBody;
    private final World world;
    private final float width, height;
    private boolean started = false;

    /**
     * @param stickmanBody obvious
     * @param width how far should the legs of the stickmanBody be apart from each other in meters
     * @param height how high should the hips be above the feet (if there were any)
     */
    public Stand(StickmanBody stickmanBody, float width, float height) {
        assert stickmanBody != null : "stickman body not specified";

        this.stickmanBody = stickmanBody;
        this.world = stickmanBody.world;
        this.width = width;
        this.height = height;
    }

    /**
     * After this call, the standing starts standing.
     * @return this instance for easier manipulation
     */
    @Override
    public Stand start() {
        started = true;
        return this;
    }

    /**
     * After this call, the standing stops standing.
     * @return this instance for easier manipulation
     */
    @Override
    public Stand stop() {
        started = false;
        return this;
    }

    /**
     * Continue standing.
     * @param dt delta time in seconds
     * @return returns null only if it has not been started yet, otherwise returns this
     */
    @Override
    public Stand update(float dt) {
        if (!started) return null;

        //FIXME: this intensity setting is functional, but really not optimal
        // I'd be the happiest person in the world if I could drop the intensity completely, but that's
        // unfortunately not possible yet (at least I don't see any better solution)
        float leftI = stickmanBody.leftBalancer().error() / (MathUtils.PI / 6) * (stickmanBody.leftBalancer().pointerPos().y > 0 ? +1 : -1);
        if (leftI > -0.7f) {
            stickmanBody.leftBalancer().setIntensity(Math.abs(leftI));
        } else {
            stickmanBody.leftBalancer().setIntensity(0);
        }
        float rightI = stickmanBody.rightBalancer().error() / (MathUtils.PI / 6) * (stickmanBody.rightBalancer().pointerPos().y > 0 ? +1 : -1);
        if (rightI > -0.7f) {
            stickmanBody.rightBalancer().setIntensity(Math.abs(rightI));
        } else {
            stickmanBody.rightBalancer().setIntensity(0);
        }

        float g = world.getGravity().len();
        float comHeight = (stickmanBody.leftBalancer().comPointerPos().x + stickmanBody.rightBalancer().comPointerPos().x) / 2;
        float bodyAngle = (stickmanBody.leftBalancer().error() + stickmanBody.rightBalancer().error()) / 2;
        float v = stickmanBody.centerOfMass.speed().dot(world.getGravity().cpy().nor().rotate(90))
                - stickmanBody.centerOfMass.angularSpeed() * comHeight * (float)Math.cos(bodyAngle) * 0.7f
                + stickmanBody.upperCenterOfMass.angularSpeed() * comHeight * (float)Math.cos(bodyAngle) * 0.3f;

        float minMov = Float.POSITIVE_INFINITY;

        // calculate the required movement for the left leg
        float leftPos = width / 2;  // position of the center of mass, not the leg
        float leftHeight = stickmanBody.leftBalancer().comPointerPos().x;
        LocoVector[] leftLVs = new LocoVector(g, 0, leftHeight, leftPos, 0).atVelocity(v);
        if (leftLVs.length > 0 && leftLVs[0].time() <= 0) {
            float mov = MathUtils.clamp(leftLVs[0].position(),
                            -0.346410162f * height,
                            +0.346410162f * height)
                        - leftPos;
            if (Math.abs(mov) < Math.abs(minMov)) {
                minMov = mov;
            }
        }

        // calculate the required movement for the right leg
        float rightPos = -width / 2;    // position of the center of mass, not the leg
        float rightHeight = stickmanBody.rightBalancer().comPointerPos().x;
        LocoVector[] rightLVs = new LocoVector(g, 0, rightHeight, rightPos, 0).atVelocity(v);
        if (rightLVs.length > 0 && rightLVs[0].time() <= 0) {
            float mov = MathUtils.clamp(rightLVs[0].position(),
                            -0.346410162f * height,
                            +0.346410162f * height)
                        - rightPos;
            if (Math.abs(mov) < Math.abs(minMov)) {
                minMov = mov;
            }
        }

        // apply the movement
        Vector2 leftVec = new Vector2(height, -width / 2);
        Vector2 rightVec = new Vector2(height, width / 2);
        if (Float.isFinite(minMov)) {
            leftVec.y -= minMov;
            rightVec.y -= minMov;
        }

        // make the vectors reachable
        for (int i = 0; i < 4; i++) {
            leftVec.x = rightVec.x = (leftVec.x + rightVec.x) / 2;
            leftVec = stickmanBody.leftBalancer().reachable(leftVec);
            rightVec = stickmanBody.rightBalancer().reachable(rightVec);
            if (leftVec.x == rightVec.x) break;
        }

        // move!
        new DirectMove(stickmanBody.leftBalancer(), leftVec, 20).setTime(dt).start().update(dt);
        new DirectMove(stickmanBody.rightBalancer(), rightVec, 20).setTime(dt).start().update(dt);

        return this;
    }

    /**
     * Standing is an Action that never finishes, so fraction() returns 0 if the action has already started,
     * or 1 if it has not.
     * @return fraction of the step
     */
    @Override
    public float fraction() {
        if (!started) {
            return 1;
        }
        return 0;
    }

}
