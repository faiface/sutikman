package faiface.stickmangine.figures.stickman;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Transform;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Specify all StickmanBody's attributes here. There is a lot to specify.
 */
public class StickmanDef {

    public World world = null;
    public short index = -1;

    /**
     * Transform specifies StickmanBody's position and angle.
     */
    public final Transform transform = new Transform(new Vector2(0, 0), -MathUtils.PI / 2);

    /**
     * StickmanBody's body measurements.
     */
    public float width = 0.1f, height = 1.8f, weight = 70f, friction = 0.3f, restitution = 0.0f;

    /**
     * StickmanBody's physical abilities.
     */
    public float strength = 1, explosiveness = 1, stiffness = 1;

    /**
     * StickmanBody's behaviour parameters.
     */
    public float stanceWidth = 0.4f, stanceHeight = 0.86f,
            walkWidth = 0.5f, walkHeight = 0.83f, walkSpeed = 1.4f,
            runWidth = 0.8f, runHeight = 0.80f, runSpeed = 2.8f;

}
