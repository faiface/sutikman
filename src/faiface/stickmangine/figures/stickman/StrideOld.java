package faiface.stickmangine.figures.stickman;

import com.badlogic.gdx.math.Vector2;
import faiface.stickmangine.balance.Balancer;
import faiface.stickmangine.balance.CenterOfMass;
import faiface.stickmangine.balance.Locomotion;
import faiface.stickmangine.movement.Action;
import faiface.stickmangine.movement.DirectMove;
import faiface.stickmangine.movement.Sequence;

//TODO: implementation
//TODO: documentation
//TODO: test out

class StrideOld implements Action {

    private CenterOfMass com;
    private Balancer standing, striding;
    private float width, height, speed;
    private boolean started = false;


    protected class Leaning implements Action {

        private DirectMove lean, liftStriding, extension;
        private Sequence liftStanding;
        private float initPos;
        private boolean started = false;

        @Override
        public Leaning start() {
            lean = new DirectMove(striding, new Vector2(Float.NaN, striding.pointerPos().y)).setTime(0).start();
            liftStriding = new DirectMove(striding, new Vector2(height, Float.NaN)).setTime(0.2f).start();
            extension = new DirectMove(standing, new Vector2(Float.NaN, width)).setSoftness(30).start();
            liftStanding = new Sequence(
                    new DirectMove(standing, new Vector2(height - 0.1f, Float.NaN)).synchronize(this, 0f, 0.3f),
                    new DirectMove(standing, new Vector2(height - 0.05f, Float.NaN)).synchronize(this, 0.7f, 1f)
            ).start();
            initPos = -striding.comPointerPos().y;
            started = true;
            return this;
        }

        @Override
        public Leaning stop() {
            started = false;
            return this;
        }

        @Override
        public Leaning update(float dt) {
            if (!started) return null;

            float g = com.world().getGravity().len();
            float h = striding.comPointerPos().x;
            float p = -striding.comPointerPos().y;
            float v = com.speed().dot(com.world().getGravity().cpy().nor().rotate(90));

            Locomotion.LocoVector nextTick = Locomotion.atTime(g, h, p, v, dt);
            lean.setTarget(new Vector2(Float.NaN, -nextTick.position()));
            lean.start().update(dt);

            extension.setTarget(new Vector2(Float.NaN, standing.worldToLocal(striding.localToWorld(striding.pointerPos().add(0, width))).y));

            float target;
            Locomotion.LocoVector[] lv = Locomotion.atVelocity(g, h, 0, speed, v);
            if (lv.length == 0 || lv[0].time() > 0) {
                target = width;
            } else {
                target = width + lv[0].position();
            }
            target = (target + p) / 2;

            lv = Locomotion.atPosition(g, h, p, v, target);
            if (lv.length > 0 && lv[lv.length-1].time() > 0) {
                extension.setSpeed(extension.target().sub(extension.point.pointerPos()).y / lv[lv.length-1].time());
            }

            extension.update(dt);

            liftStriding.update(dt);
            liftStanding.update(dt);

            standing.setIntensity(1);

            return this;
        }

        @Override
        public float fraction() {
            if (!started) return 1;

            float g = com.world().getGravity().len();
            float h = striding.comPointerPos().x;
            float p = -striding.comPointerPos().y;
            float v = com.speed().dot(com.world().getGravity().cpy().nor().rotate(90));

            float target;
            Locomotion.LocoVector[] lv = Locomotion.atVelocity(g, h, 0, speed, v);
            if (lv.length == 0 || lv[0].time() > 0) {
                target = width;
            } else {
                target = width + lv[0].position();
            }
            target = (target + p) / 2;

            return (p - initPos) / (target - initPos);
        }

    }


    protected class Striding implements Action {

        private DirectMove vault, liftStanding, stride;
        private Sequence liftStriding;
        private float initPos;
        private boolean started = false;

        private float prevPos = 0;

        @Override
        public Striding start() {
            vault = new DirectMove(standing, new Vector2(Float.NaN, standing.pointerPos().y)).setTime(0).start();
            liftStanding = new DirectMove(standing, new Vector2(height, Float.NaN)).setTime(0.1f).start();
            stride = new DirectMove(striding, new Vector2(Float.NaN, 0)).setSoftness(30).start();
            liftStriding = new Sequence(
                    new DirectMove(striding, new Vector2(height - 0.1f, Float.NaN)).synchronize(this, 0f, 0.3f)
            ).start();
            initPos = -standing.comPointerPos().y;
            prevPos = initPos;  //FIXME:TEMP
            started = true;
            return this;
        }

        @Override
        public Striding stop() {
            started = false;
            return this;
        }

        @Override
        public Striding update(float dt) {
            if (!started) return null;

            float g = com.world().getGravity().len();
            float h = standing.comPointerPos().x;
            float p = prevPos;// -standing.comPointerPos().y;
            float v = com.speed().dot(com.world().getGravity().cpy().nor().rotate(90));

            Locomotion.LocoVector nextTick = Locomotion.atTime(g, h, p, v, dt);
            vault.setTarget(new Vector2(Float.NaN, -nextTick.position()));
            vault.start().update(dt);

            Locomotion.LocoVector[] lv = Locomotion.atPosition(g, h, p, v, 0);
            if (lv.length > 0 && lv[lv.length-1].time() > 0) {
                stride.setSpeed(stride.target().sub(stride.point.pointerPos()).y / lv[lv.length-1].time());
            }

            //stride.update(dt);

            liftStanding.update(dt);
            liftStriding.update(dt);

            striding.setIntensity(1);

            //FIXME:TEMP
            //System.out.println(h);
            prevPos = nextTick.position();

            return this;
        }

        @Override
        public float fraction() {
            if (!started) return 1;

            float g = com.world().getGravity().len();
            float h = standing.comPointerPos().x;
            float p = -standing.comPointerPos().y;
            float v = com.speed().dot(com.world().getGravity().cpy().nor().rotate(90));

            return (p - initPos) / (0 - initPos);
        }

    }


    public StrideOld(CenterOfMass com, Balancer standing, Balancer striding, float width, float height, float speed) {
        this.com = com;
        this.standing = standing;
        this.striding = striding;
        this.width = width;
        this.height = height;
        this.speed = Math.abs(speed);
    }

    private Sequence sequence;

    @Override
    public StrideOld start() {
        sequence = new Sequence(new Leaning(), new Striding()).start();
        return this;
    }

    @Override
    public StrideOld stop() {
        return this;
    }

    @Override
    public StrideOld update(float dt) {
//        standing.setAcceleration(new Vector2((float)Math.sin(standing.error()) * 10f, 0));
//        striding.setAcceleration(new Vector2((float)Math.sin(striding.error()) * 10f, 0));
        sequence.update(dt);
        return this;
    }

    @Override
    public float fraction() {
        return sequence.fraction();
    }

}
