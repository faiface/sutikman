package faiface.stickmangine;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Stick is the basic building block of stick figures. It's The Stick!!!
 */
public class Stick implements Segment {

    private final Body body;
    private final Vector2 localA, localB;

    public final float length, width, friction, restitution;

    /**
     * @param def specify all the properties of the stick here
     */
    public Stick(StickDef def) {
        assert def.world != null : "world must be specified";

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = def.dynamic ? BodyDef.BodyType.DynamicBody : BodyDef.BodyType.StaticBody; //FIXME:TEMPORARY
        bodyDef.position.set(def.position);
        bodyDef.angle = def.angle;

        body = def.world.createBody(bodyDef);
        body.setUserData(def.userData);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = def.density;
        fixtureDef.friction = def.friction;
        fixtureDef.restitution = def.restitution;
        fixtureDef.filter.categoryBits = def.filter.categoryBits;
        fixtureDef.filter.maskBits = def.filter.maskBits;
        fixtureDef.filter.groupIndex = def.filter.groupIndex;

        if (def.isCircle()) {  // segment is just a circle now
            CircleShape circle = new CircleShape();
            circle.setPosition(new Vector2(0, 0));
            circle.setRadius(def.width / 2);
            fixtureDef.shape = circle;
            body.createFixture(fixtureDef);
            localA = new Vector2(0, 0);
            localB = new Vector2(0, 0);
            circle.dispose();
        } else {                // this is a fully featured segment
            PolygonShape rectangle = new PolygonShape();
            rectangle.setAsBox(def.length / 2, def.width / 2);
            fixtureDef.shape = rectangle;
            body.createFixture(fixtureDef);
            CircleShape circle = new CircleShape();
            circle.setPosition(new Vector2(-def.length / 2, 0));
            circle.setRadius(def.width / 2);
            fixtureDef.shape = circle;
            body.createFixture(fixtureDef);
            localA = circle.getPosition().cpy();
            circle.setPosition(new Vector2(def.length / 2, 0));
            body.createFixture(fixtureDef);
            localB = circle.getPosition().cpy();
            rectangle.dispose();
            circle.dispose();
        }

        length = def.length;
        width = def.width;
        friction = def.friction;
        restitution = def.restitution;
    }

    /**
     * @return local coordinates of point A of the stick
     */
    @Override
    public Vector2 localA() {
        return localA.cpy();
    }

    /**
     * @return world coordinates of point A of the stick
     */
    @Override
    public Vector2 A() {
        return body.getWorldPoint(localA).cpy();
    }

    /**
     * "Teleports" the segment to the specified position; rotation will be kept
     * @param pos world coordinates
     */
    @Override
    public void setA(Vector2 pos) {
        body.setTransform(body.getPosition().sub(A().sub(pos)), body.getAngle());
        body.setAwake(true);
    }

    /**
     * @return local coordinates of point B of the stick
     */
    @Override
    public Vector2 localB() {
        return localB.cpy();
    }

    /**
     * @return world coordinates of point B of the stick
     */
    @Override
    public Vector2 B() {
        return body.getWorldPoint(localB).cpy();
    }

    /**
     * Teleports the segment to the specified position; rotation and size will be kept
     * @param pos world coordinates
     */
    @Override
    public void setB(Vector2 pos) {
        body.setTransform(body.getPosition().sub(B().sub(pos)), body.getAngle());
        body.setAwake(true);
    }

    /**
     * @return distance between points A and B of the stick
     */
    @Override
    public float length() {
        return length;
    }

    /**
     * @return angular orientation of the stick in radians
     */
    @Override
    public float angle() {
        return body.getAngle();
    }

    /**
     * @return physical body of the stick
     */
    @Override
    public Body body() {
        return body;
    }

}
